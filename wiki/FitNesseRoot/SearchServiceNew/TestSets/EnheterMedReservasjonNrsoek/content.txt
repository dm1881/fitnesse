!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values | contain reservation? | actual values? |
| 33462260 | Voice | Found | |
| 64942882 | Voice | Found | |
| 51972700 | Voice | Not found | |
| 97987333 | Voice | Not found | |
| 91992505 | Online | Not found | |
| 96626060 | Online | Not found | |
| 93809261 | Online | Not found | |
| 95151836 | Voice | Found | |
| 46894183 | Voice | Found | |
| 41467783 | Voice | Found | |
| 22821060 | Voice | Not found | |
| 94850622 | Voice | Found | |
| 90415939 | Voice | Found | |
| 99297774 | Voice | Found | |
| 45471962 | Voice | Found | |
| 41430231 | Voice | Found | |
| 45259747 | Voice | Found | |
| 73109558 | Voice | Found | |
| 91823971 | Voice | Found | |
| 47904955 | Voice | Found | |
| 90182974 | Voice | Found | |
| 28012555 | Voice | Not found | |
| 94893225 | Voice | Found | |
| 48101402 | Voice | Found | |
| 40494202 | Voice | Found | |
| 95126959 | Voice | Found | |
| 91196234 | Voice | Found | |
| 90829105 | Voice | Found | |
| 81573700 | Voice | Found | |
| 47311152 | Voice | Found | |
| 91247028 | Online | Not found | |
| 90574899 | Online | Not found | |
| 97020163 | Online | Not found | |
| 95433554 | Online | Not found | |
| 91247028 | Online | Not found | |
| 91705153 | Online | Not found | |
| 90196012 | Online | Not found | |
| 91197855 | Online | Not found | |
| 95858277 | Online | Not found | |
| 95027384 | Online | Not found | |
| 95417390 | Online | Not found | |



Bedrift: Kontaktpunkter med reservasjon innenfor en enhet som ikke er fullreservert

!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values | contain reservation? | actual values? |
| 46836868 | Voice | Found | |
| 90813432 | Voice | Found | |
| 95286051 | Voice | Found | |
| 21576769 | Voice | Found | |
| 21602600 | Voice | Found | |
| 21006362 | Voice | Found | |
| 52021407 | Voice | Found | |
| 64002763 | Voice | Found | |
| 95276965 | Voice | Found | |
| 21073257 | Voice | Found | |
| 67258134 | Voice | Found | |
| 90791470 | Voice | Found | |
| 52024960 | Voice | Found | |
| 52022529 | Voice | Found | |
| 21578708 | Voice | Found | |
| 21053337 | Voice | Found | |
| 52020511 | Voice | Found | |
| 90793121 | Voice | Found | |
| 51215880 | Voice | Found | |
| 51214639 | Voice | Found | |
| 90018442 | Voice | Found | |
| 35704823 | Voice | Found | |
| 47990756 | Voice | Found | |
| 90411548 | Voice | Found | |
| 47982169 | Voice | Found | |
| 33296968 | Voice | Found | |
| 90473278 | Voice | Found | |
| 51214641 | Voice | Found | |
| 48197210 | Voice | Found | |
| 91890146 | Voice | Found | |
| 48104586 | Voice | Found | |
| 90471679 | Voice | Found | |
| 47984205 | Voice | Found | |
| 47990383 | Voice | Found | |
| 45640064 | Voice | Found | |
| 90939416 | Voice | Found | |
| 78608388 | Voice | Found | |
| 97419288 | Voice | Found | |
| 47990556 | Voice | Found | |
| 35704813 | Voice | Found | |
| 97732798 | Voice | Found | |
| 21095008 | Voice | Found | |
| 91336158 | Voice | Found | |
| 47982432 | Voice | Found | |
| 69708195 | Voice | Found | |
| 21095040 | Voice | Found | |
| 94847307 | Voice | Found | |
| 22910698 | Voice | Found | |
| 73109508 | Voice | Found | |
| 47983159 | Voice | Found | |
| 77284622 | Voice | Found | |
| 47988576 | Voice | Found | |
| 71402905 | Voice | Found | |
| 90959740 | Voice | Found | |
| 90664042 | Voice | Found | |
| 47982965 | Voice | Found | |
| 91110331 | Voice | Found | |
| 95143660 | Voice | Found | |
| 91836955 | Voice | Found | |
| 90512377 | Voice | Found | |
| 97778215 | Voice | Found | |
| 91182690 | Voice | Found | |
| 97656330 | Voice | Found | |
| 41404265 | Voice | Found | |
| 95030342 | Voice | Found | |
| 90923129 | Voice | Found | |
| 91684587 | Voice | Found | |
| 95437864 | Voice | Found | |
| 97056683 | Voice | Found | |
| 92424999 | Voice | Found | |
| 97729054 | Voice | Found | |
| 97081848 | Voice | Found | |
| 99023906 | Voice | Found | |
| 97420170 | Voice | Found | |
| 97623211 | Voice | Found | |
| 92268025 | Voice | Found | |
| 95042058 | Voice | Found | |
| 91916203 | Voice | Found | |
| 99488437 | Voice | Found | |
| 95030333 | Voice | Found | |
| 95873105 | Voice | Found | |
| 95742298 | Voice | Found | |
| 41428136 | Voice | Found | |
| 95265016 | Voice | Found | |
| 97626425 | Voice | Found | |
| 98838623 | Voice | Found | |
| 92805686 | Voice | Found | |
| 92667134 | Voice | Found | |
| 95830269 | Voice | Found | |
| 91609358 | Voice | Found | |
| 91766589 | Voice | Found | |
| 48997026 | Voice | Found | |
| 90197011 | Voice | Found | |
| 113 | Voice | Found | |
| 91512657 | Voice | Found | |
| 90973139 | Voice | Found | |
| 95784759 | Voice | Found | |
| 91680487 | Voice | Found | |
| 48031907 | Voice | Found | |
| 95161973 | Voice | Found | |
| 97527558 | Voice | Found | |
| 97755555 | Voice | Found | |
| 48260044 | Voice | Found | |
| 47679277 | Voice | Found | |
| 94892603 | Voice | Found | |
| 95018450 | Voice | Found | |
| 97728271 | Voice | Found | |
| 95755919 | Voice | Found | |
| 99491256 | Voice | Found | |
| 94794771 | Voice | Found | |
| 41735824 | Voice | Found | |
| 91116429 | Voice | Found | |
| 90071844 | Voice | Found | |
| 94192192 | Voice | Found | |
| 91695468 | Voice | Found | |
| 91675156 | Voice | Found | |
| 95234877 | Online | Not found | |
| 41627065 | Online | Not found | |
| 73890700 | Online | Not found | |
| 48127090 | Online | Not found | |
| 90998617 | Online | Not found | |
| 95090381 | Online | Not found | |
| 90705669 | Online | Not found | |
| 91377759 | Online | Not found | |
| 93445025 | Online | Not found | |
| 56514635 | Online | Not found | |
| 94167974 | Online | Not found | |
| 120 | Online | Not found | |
| 94983033 | Online | Not found | |
| 91672393 | Online | Not found | |
| 91737315 | Online | Not found | |
| 99267088 | Online | Not found | |
| 48266271 | Online | Not found | |
| 97181900 | Online | Not found | |
| 95124310 | Online | Not found | |
| 48225208 | Online | Not found | |
| 95081430 | Online | Not found | |
| 95497547 | Online | Not found | |
| 81533133 | Online | Not found | |
| 33003600 | Online | Not found | |
| 46896850 | Online | Not found | |
| 95823909 | Online | Not found | |
| 98094169 | Online | Not found | |
| 41646223 | Online | Not found | |
| 90034592 | Online | Not found | |
| 90259135 | Online | Not found | |
| 55388800 | Online | Not found | |
| 91693178 | Online | Not found | |
| 96506295 | Online | Not found | |
| 94973858 | Online | Not found | |
| 97552358 | Online | Not found | |
| 91100059 | Online | Not found | |
| 180 | Online | Not found | |
| 99575990 | Online | Not found | |
| 45496996 | Online | Not found | |
| 91724986 | Online | Not found | |
| 46447088 | Online | Not found | |
| 48899678 | Online | Not found | |
| 95876601 | Online | Not found | |
| 46842572 | Online | Not found | |
| 48275230 | Online | Not found | |
| 91114600 | Online | Not found | |
| 97020162 | Online | Not found | |
| 73890700 | Online | Not found | |
| 90607690 | Online | Not found | |
| 47486823 | Online | Not found | |
| 91848551 | Online | Not found | |
| 08110 | Online | Not found | |
| 95992299 | Online | Not found | |
| 70163070 | Online | Not found | |
| 33003600 | Online | Not found | |
| 99308191 | Online | Not found | |
| 48189469 | Online | Not found | |
| 46807204 | Online | Not found | |
| 99099490 | Online | Not found | |
| 90649044 | Online | Not found | |
| 90195115 | Online | Not found | |
| 95072511 | Voice | Found | |
| 63936940 | Voice | Found | |
| 91159910 | Voice | Found | |
| 24154715 | Voice | Found | |
| 35961875 | Voice | Found | |
| 81559100 | Voice | Found | |
| 62415320 | Voice | Found | |
| 41632128 | Voice | Found | |
| 22974700 | Voice | Found | |
| 81045277 | Voice | Found | |
| 91306744 | Voice | Found | |
| 41536388 | Voice | Found | |
| 62539612 | Voice | Found | |
| 22161510 | Voice | Found | |
| 22682807 | Voice | Found | |
| 22728470 | Voice | Found | |
| 22613031 | Voice | Found | |
| 75568521 | Voice | Found | |

Privat: Kontaktpunkter med reservasjon innenfor en enhet som ikke er fullreservert

!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values | contain reservation? | actual values? |
| 94141849 | Voice | Found | |
| 91627606 | Voice | Found | |
| 97960587 | Voice | Found | |
| 91528044 | Voice | Found | |
| 91596236 | Voice | Found | |
| 48023944 | Voice | Found | |
| 91735134 | Voice | Found | |
| 41213525 | Voice | Found | |
| 94131040 | Voice | Found | |
| 90061480 | Voice | Found | |
| 97475739 | Voice | Found | |
| 48098240 | Voice | Found | |
| 99234745 | Voice | Found | |
| 97005815 | Voice | Found | |
| 99304077 | Voice | Found | |
| 95339611 | Voice | Found | |
| 41242739 | Voice | Found | |
| 90661102 | Voice | Found | |
| 94838081 | Voice | Found | |
| 48176966 | Voice | Found | |
| 92496139 | Voice | Found | |
| 98426867 | Voice | Found | |
| 99321675 | Voice | Found | |
| 94050610 | Voice | Found | |
| 90888755 | Voice | Found | |
| 41411507 | Voice | Found | |
| 48252017 | Voice | Found | |
| 91001834 | Voice | Found | |
| 91771848 | Voice | Found | |
| 91831137 | Voice | Found | |
| 99346794 | Voice | Found | |
| 99297774 | Voice | Found | |
| 90182224 | Voice | Found | |
| 40461989 | Voice | Found | |
| 90175831 | Voice | Found | |
| 94892610 | Voice | Found | |
| 48084093 | Voice | Found | |
| 98230619 | Voice | Found | |
| 45289357 | Voice | Found | |
| 95148135 | Voice | Found | |
| 41855781 | Voice | Found | |
| 99552711 | Voice | Found | |
| 90803907 | Voice | Found | |
| 95760854 | Voice | Found | |
| 97165416 | Voice | Found | |
| 41212709 | Voice | Found | |
| 95878760 | Voice | Found | |
| 95992746 | Voice | Found | |
| 94161687 | Voice | Found | |
| 99310163 | Voice | Found | |
| 41546635 | Voice | Found | |
| 45176598 | Voice | Found | |
| 95032086 | Voice | Found | |
| 90619112 | Voice | Found | |
| 95491550 | Voice | Found | |
| 97744401 | Voice | Found | |
| 41765579 | Voice | Found | |
| 90819759 | Voice | Found | |
| 97191500 | Voice | Found | |
| 97884566 | Voice | Found | |
| 93663525 | Voice | Found | |
| 46943787 | Voice | Found | |
| 99292840 | Voice | Found | |
| 97760826 | Voice | Found | |
| 99573336 | Voice | Found | |
| 90235986 | Voice | Found | |
| 97898908 | Voice | Found | |
| 41687900 | Voice | Found | |
| 90684291 | Voice | Found | |
| 97640842 | Voice | Found | |
| 41277775 | Voice | Found | |
| 97165937 | Voice | Found | |
| 48038409 | Voice | Found | |
| 99249744 | Voice | Found | |
| 41226561 | Voice | Found | |
| 99464278 | Voice | Found | |
| 47847583 | Voice | Found | |
| 90988485 | Voice | Found | |
| 90769058 | Voice | Found | |
| 48234457 | Voice | Found | |
| 48242895 | Voice | Found | |
| 90854038 | Voice | Found | |
| 98417531 | Voice | Found | |
| 90177560 | Voice | Found | |
| 90825344 | Voice | Found | |
| 94150881 | Voice | Found | |
| 95030306 | Voice | Found | |
| 98879165 | Voice | Found | |
| 95244760 | Voice | Found | |
| 94292592 | Voice | Found | |
| 45141414 | Voice | Found | |
| 93298141 | Voice | Found | |
| 94152181 | Voice | Found | |
| 40483762 | Voice | Found | |
| 98281141 | Voice | Found | |
| 47256919 | Voice | Found | |
| 96626060 | Voice | Found | |
| 95748941 | Voice | Found | |
| 97648032 | Voice | Found | |
| 48262452 | Voice | Found | |
| 46962232 | Voice | Found | |
| 97688612 | Voice | Found | |
| 99105636 | Voice | Found | |
| 93809261 | Voice | Found | |
| 90643712 | Voice | Found | |
| 94269852 | Voice | Found | |
| 94501064 | Voice | Found | |
| 99449111 | Voice | Found | |
| 95063313 | Voice | Found | |
| 99223354 | Voice | Found | |
| 99480227 | Voice | Found | |
| 40287109 | Voice | Found | |
| 90852269 | Voice | Found | |
| 92635726 | Voice | Found | |
| 94888059 | Voice | Found | |
| 45425983 | Voice | Found | |
| 47648128 | Voice | Found | |
| 91361884 | Voice | Found | |
| 95030371 | Voice | Found | |
| 92226990 | Voice | Found | |
| 97177868 | Voice | Found | |
| 91612372 | Voice | Found | |
| 48001347 | Voice | Found | |
| 94897058 | Voice | Found | |
| 97166336 | Voice | Found | |
| 47623144 | Voice | Found | |
| 92806421 | Voice | Found | |
| 47340541 | Voice | Found | |
| 41466324 | Voice | Found | |
| 46963796 | Voice | Found | |
| 41635391 | Voice | Found | |
| 41760443 | Voice | Found | |
| 97019763 | Voice | Found | |
| 91340131 | Voice | Found | |
| 97043964 | Voice | Found | |
| 90760651 | Voice | Found | |
| 97494982 | Voice | Found | |
| 41621895 | Voice | Found | |
| 90547246 | Voice | Found | |
| 90968949 | Voice | Found | |
| 91375256 | Voice | Found | |
| 41848402 | Voice | Found | |
| 48226492 | Voice | Found | |
| 46967363 | Voice | Found | |
| 90941062 | Voice | Found | |
| 93480884 | Voice | Found | |
| 95074920 | Voice | Found | |
| 47628002 | Voice | Found | |
| 94987133 | Voice | Found | |
| 98659505 | Voice | Found | |
| 99520662 | Voice | Found | |
| 90063205 | Voice | Found | |
| 90521369 | Voice | Found | |
| 94191996 | Voice | Found | |
| 90913511 | Voice | Found | |
| 41548074 | Voice | Found | |
| 41469897 | Voice | Found | |
| 94033535 | Voice | Found | |
| 41633794 | Voice | Found | |
| 91991520 | Voice | Found | |
| 97576712 | Voice | Found | |
| 41277329 | Voice | Found | |
| 93652695 | Voice | Found | |
| 90824494 | Voice | Found | |
| 91878526 | Online | Not found | |
| 90096660 | Online | Not found | |
| 95365073 | Online | Not found | |
| 95104834 | Online | Not found | |
| 46929969 | Online | Not found | |
| 90992553 | Online | Not found | |
| 45607975 | Online | Not found | |
| 90591320 | Online | Not found | |
| 48042329 | Online | Not found | |
| 45025510 | Online | Not found | |
| 90686448 | Online | Not found | |
| 97106320 | Online | Not found | |
| 90981103 | Online | Not found | |
| 95860119 | Online | Not found | |
| 90666203 | Online | Not found | |
| 46807404 | Online | Not found | |
| 95249133 | Online | Not found | |
| 45223838 | Online | Not found | |
| 41843997 | Online | Not found | |
| 91767813 | Online | Not found | |
| 92996003 | Online | Not found | |
| 90222218 | Online | Not found | |
| 90218342 | Online | Not found | |
| 91711105 | Online | Not found | |
| 90916251 | Online | Not found | |
| 99405115 | Online | Not found | |
| 90666346 | Online | Not found | |
| 91705153 | Online | Not found | |
| 41518580 | Online | Not found | |
| 90033332 | Online | Not found | |
| 41531382 | Online | Not found | |
| 91884171 | Online | Not found | |
| 41531301 | Online | Not found | |
| 97552352 | Online | Not found | |
| 91323882 | Online | Not found | |
| 97422554 | Online | Not found | |
| 41402555 | Online | Not found | |
| 93452647 | Online | Not found | |
| 94148704 | Online | Not found | |
| 95475981 | Online | Not found | |
| 98290912 | Online | Not found | |
| 90164942 | Online | Not found | |
| 93461184 | Online | Not found | |
| 94854382 | Online | Not found | |
| 95983118 | Online | Not found | |
| 90162250 | Online | Not found | |
| 95077628 | Online | Not found | |
| 41507862 | Online | Not found | |
| 47676680 | Online | Not found | |
| 91570569 | Online | Not found | |
| 90516797 | Online | Not found | |
| 97688268 | Online | Not found | |
| 95819741 | Online | Not found | |
| 98290911 | Online | Not found | |
| 95093189 | Online | Not found | |
| 41935230 | Online | Not found | |
| 97637114 | Online | Not found | |
| 93077188 | Online | Not found | |
| 90616016 | Online | Not found | |
| 41841228 | Online | Not found | |
| 98220739 | Online | Not found | |
| 93211769 | Online | Not found | |
| 95944027 | Online | Not found | |
| 47714463 | Online | Not found | |
| 41588663 | Online | Not found | |
| 97050017 | Online | Not found | |
| 97089284 | Online | Not found | |
| 91133739 | Online | Not found | |
| 95820096 | Online | Not found | |
| 99570051 | Online | Not found | |
| 97769121 | Online | Not found | |
| 99509159 | Online | Not found | |
| 97964950 | Online | Not found | |
| 41412185 | Online | Not found | |
| 91684441 | Online | Not found | |
| 47848670 | Online | Not found | |
| 92884928 | Online | Not found | |
| 94338911 | Online | Not found | |
| 95180824 | Online | Not found | |
| 91396833 | Online | Not found | |
| 90256699 | Online | Not found | |
| 98241982 | Online | Not found | |
| 91718540 | Online | Not found | |
| 90562808 | Online | Not found | |
| 48304465 | Online | Not found | |
| 95913810 | Online | Not found | |
| 97058588 | Online | Not found | |
| 91799924 | Online | Not found | |
| 90089015 | Online | Not found | |
| 90940914 | Online | Not found | |
| 93059416 | Online | Not found | |
| 99540356 | Online | Not found | |
| 90023865 | Online | Not found | |
| 95041750 | Online | Not found | |
| 47713958 | Online | Not found | |
| 48143975 | Online | Not found | |
| 95296809 | Online | Not found | |
| 94167386 | Online | Not found | |
| 98235962 | Online | Not found | |
| 46965430 | Online | Not found | |

!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values | contain reservation? | actual values? |
| 99 29 77 74 | Voice | Found | |
| 454 71 962 | Voice | Found | |
| 4 5 2 5 9 7 4 7 | Voice | Found | |
| +4747869340 | Voice | Found | |
| +47 479 88 803 | Voice | Found | |
| +47 96 79 94 50 | Voice | Found | |
| 0047 47990003 | Voice | Found | |
| 47 90519881 | Voice | Found | |
| 52 02 14 07 | Voice | Found | |
| 64 0 0 2 7 63 | Voice | Found | |
| 952 76 965 | Voice | Found | |
| 3335 5345 | Voice | Found | |
| 520 26 472 | Voice | Found | |
| 41 64 62 23 | Online | Not found | |
| 577 22 991 | Online | Not found | |
| 9955 2711 | Voice | Found | |
| 90 803 907 | Voice | Found | |
| 957 6 0 854 | Voice | Found | |
| 479716 5416 | Voice | Found | |
| 004741212709 | Voice | Found | |
| +95878760 | Voice | Found | |
| 95992746 | Voice | Found | |
