!|${SEARCH_API} ${TABLE_NAME}|
|query |expected |checkTop |result? |
|brødr. bjugstad rudshøgda|brødrene bjugstad as|1||*Søket gir 0-treff uten synonymet|
|Brødr Bjørklund|Brødrene Bjørklund AS|1||
|brødr dahl førde|Brødrene Dahl AS|1||*Søket gir 0-treff uten synonymet|
|brødrene palmesen fauske|brødr palmesen ans|1||*Søket gir 0-treff uten synonymet|
|Brødrene Sunde AS| Brødr. Sunde AS|8||
|brødrene jacobsen handel haugesund|Brødr Jacobsen handel AS (Expert elektroland)|1||*Søket gir 0-treff uten synonymet|
|brødr jacobsen haugesund|Brødr Jacobsen handel AS (Expert elektroland)|1||Verifisere at data finnes|
|diekonale sykehus, oslo|Lovisenberg Diakonale sykehus AS ¤ Hospice Lovisenberg|6||*Lovisenberg diakonale sykehus i juridisk navn på Hospice lovisenberg. Treff i adresse kommer før denne|
|haraldsplass diekonale|Haraldsplass diakonale sykehus AS|2||*Høyskole'n og sykehuset|
|diekonale stiftelse, oslo|Fagerborg diakonale stiftelse|1||*Søket gir 0-treff uten synonymet|
|gaular avløser |Gaular Avløysar|1||fungerer uten|
|avløser sande i sunnfjord|Gaular Avløysar|1||mangler synonym, må legge inn|
|avløysar lysaker|bil avløser|3||mangler synonym, må legge inn|
|fræna ungdomsskule|Fræna ungdomsskole|1||
|begravelsesbyrå, syvde|Vanylven Gravferdsbyrå|1||tatt vekk synonym. fungerer uten|
|hafrsfjord billedbyrå|Fotonova Bildebyrå|1||tatt vekk synonym. fungerer uten|
|breband sandefjord|Sandefjord bredbånd|1||tatt vekk synonym. fungerer uten|
|klepp bredbånd, kleppe|Klepp Breiband|1||tatt vekk synonym. fungerer uten|
|alarmsystem båt stavanger|Autotrap AS|1||"alarmsystemer båter" i mco_text1. Fungerer med synonym, 0-treff uten. mangler synonym|
|Buholmen båter|Buholmen båt AS|1||
|båtmesse, ålesund|Sparebanken Møre Arena|1||Har "båtmesser" som solgt søkeord. Vil ikke fungere når salg er gått ut|
|båtmesser stavanger|Stavanger Båtmesse AS|1||0-treff uten, ok med synonym. Fungerer uten synonym dersom søket er "stavanger båtmesser"|
|stavern båttur|TeMa havfiske ANS|2||Har "båtturer" i mco_text1|
|TeMa båttur|TeMa havfiske ANS|1||Har "båtturer" i mco_text1|
|båtturer vestfossen|Båttur Vestfossen-Eidsfoss|1||0-treff uten, ok med synonym. |
|dr dyregod|Doktor Dyregod AS|1||
|kirken sos døgn, lier|Kirkens Sos i Buskerud|1||"døgnåpen" i mco_text1|
|netcom døgn|Netcom AS|1||"døgnåpen" i na_name3|
|frisco blomster døgn|Frisco Blomster og begravelsesbyrå|1||
|jessheim dps døgnvakt|Jessheim DPS, seksjon Døgn Elvestad|2||
|nordhordaland legevakt døgnvakt|Nordhordaland legevakt|1||
|advokat døgnåpen oslo|Advokatene Gjendem & Mæland|1||"døgn åpent" i mco_text2|
|dyreklinikk døgnåpen vestfold|Dyreklinikk 24 døgnvakt - Dyrlegen senter|1||
|fjerde|quarts|9999||fant ikkje data|
|h & m elektro, sandnes|HM Elektro Automatikk AS|2||
|handelshøgskolen, oslo|handelshøyskolen BI|1||
|handelshøyskolen stavanger|BI Stavanger|1||handelshøgskolen bi' i eso|
|tranøy helsesenter|Tranøy helse- og sosialsenter|1||
|svingen helsesenter|svingen legesenter|1||
|stavanger klinikk|Stavanger spa clinique ANS|1||
|skin klinikk, sandnes|SKIN clinique|1||gir 0-treff uten synonym|
|kristines klinikk|Kristines clinique AS|1||gir 0-treff uten synonym|
|kommunelege hægebostad|Kommunelækjaren i Hægebostad|1||
|kommunelege Eggesbøvegen herøy|Kommunelækjaren i Herøy|2||
|kommunelege vadsø|kommuneleger i vadsø|1||
|kommuneleger vang i valdres|Kommunelækjaren i Vang|1||
|kommuneleger leknes|Kommunelege i Vestvågøy|1||
|kommunelege stordal|kommunelækjaren i stordal|1||tatt vekk|
|kommunelækjarar valle|kommunelegen i Valle|1||tatt vekk|
|osen legevaktsentralen|Osen legevakt|1||
|sola lufthamn|Avinor Stavanger lufthavn Sola|1||
|macdonalds oslo|McDonald's Norge AS|1||flagge hovedkontoret?|
|sandefjord møbel|Mine Møbler AS|1||Komme på 5-plass uten synonymet|
|hovden møbler|Hovden Møbel AS|1||Komme på 2-plass uten synonymet|
|drammen opplærings senter|drammen opplæringssenter|1||Gir 0-treff uten synonymet|
|kvantum senter|kvantum kjøpesenter as|1||
|lagunen senter|lagunen storsenter|1||
|senteret halden|halden storsenter|4||
|bjørneparken senteret|Bjørneparken kjøpesenter|1||
|service kommunikasjon, risør|sørvis kommunikasjon as|1||
|mobakk bil og sørvis|mobakk bil & service as|1||
|ole økset|ole erlend øxseth|1||Er lite utvalg|
|asbjørg økset|asbjørg øxseth|1||
|comfort, oslo|comfortbutikken skøyen AS|11||avd skøyen skal komme før treff i juridisk navn her|
|Hølonda maskin|Hølonda maskinutleie|10||
|Karmøy lift & maskin AS|Karmøy lift & maskinutleie AS|10||
|Numedal maskin|Numedal Maskinutleie AS|10||
|Østeig Minimaskin|Østeig Minimaskinutleie|10||
|Ekspert maskin avd. Oslo|Ekspert Maskinutleie avd. Oslo|10||
|Haugen maskin|Haugen Maskinutleie|10||
|Golden Lift & maskin|Golden Lift & Maskinutleie|10||
|Ekspert maskin AS|Ekspert Maskinutleie AS|10||
|Bjørn Berg maskin|Bjørn Berg Maskinutleie|10||
|dyrendahl maskin|Dyrendahl Maskinutleie AS|1||
|Mysen maskin ANS|Mysen Maskinutleie ANS|2||
|Hertz maskin Safelift|Hertz Maskinutleie Safelift|2||
|Myrvik maskin AS|Myrvik Maskinutleie AS|2||
|Hæhre maskin AS|Hæhre Maskinutleie AS|2||
|Brattås transport & maskin AS|Brattås transport & Maskinutleie AS|2||
|Hego maskin AS|Hego Maskinutleie AS|2||
|Hertz maskin Safelift|Hertz Maskinutleie Safelift|2||
|Vian maskin|Vian Maskinutleie Vincent Nilsson|2||
|Minimax maskin AS|Minimax Maskinutleie AS|2||
|Sunndal maskin AS|Sunndal Maskinutleie AS|2||
|Hagens maskin|Hagens Maskinutleie|2||
|Kongsberg Lift og maskin AS|Kongsberg Lift og Maskinutleie AS|2||
|KBE maskin AS|KBE Maskinutleie AS|2||
|Lofthus maskin AS|Lofthus Maskinutleie AS|2||
|Eriksen maskin AS|Eriksen Maskinutleie AS|5||
|Strand maskin AS|Strand Maskinutleie AS|2||
|Romerike maskin AS|Romerike Maskinutleie AS|2||
|Ganddal maskin AS|Ganddal Maskinutleie AS|2||
|Persøy Lift og maskin AS|Persøy Lift & Maskinutleie AS|2||
|haug transport og maskin AS|Haug transport & Maskinutleie|2||
|Hurum maskin|Hurum Maskinutleie|2||
|Worren Kran & maskin AS|Worren Kran & Maskinutleie AS|2||
|Oddvar Flatebø maskin AS|Oddvar Flatebø Maskinutleie AS|2||
|Brødrene Harsjøens maskin|Brødrene Harsjøens Maskinutleie|2||
|Kåtorp maskin|Kåtorp Maskinutleie|2||
|Hertz maskin Safelift|Hertz Maskinutleie Safelift|2||
|Tesko maskin|Tesko Maskinutleie|2||
|Bergs maskin|Bjørn Berg Maskinutleie |3||
|Maxrent maskin AS|Maxrent Maskinutleie AS|2||
|RXI maskin|RXI Maskinutleie|2||
|RXI maskin|RXI Maskinutleie|2||
|Askøy maskin|Askøy Maskinutleie Hein Fromreide|2||
|Frosta maskin DA|Frosta Maskinutleie DA|2||
|Høvåg maskin AS|Høvåg Maskinutleie AS|2||
|Haddeland maskin|Haddeland Maskinutleie|2||
|Hertz maskin Safelift|Hertz Maskinutleie Safelift|2||
|Ole-Petter Holene maskin|Ole-Petter Holene Maskinutleie|2||
|Vesterdal Bjørn maskin|Vesterdal Bjørn Maskinutleie|2||
|Hertz maskin Safelift|Hertz Maskinutleie Safelift|2||
|Pedersen maskin|Pedersen Maskinutleie|4||
|Wannberg maskin AS|Wannberg Maskinutleie AS|2||
|Drammen maskin AS|Drammen maskin utleie AS|3||
|Vedlikehold & maskin|Vedlikehold & Maskinutleie|2||
|Toten maskin V/ Geir Gravdahl|Toten Maskinutleie V/ Geir Gravdahl|2||
|Østlandske maskin AS|Østlandske Maskinutleie AS|2||
|Homme maskin|Homme Maskinutleie|2||
|Bas maskin AS|Bas Maskinutleie AS|7||
|Øksnes maskin AS|Øksnes Maskinutleie AS|3||
|Bas maskin avd. Oslo/Akershus|Bas Maskinutleie avd. Oslo/Akershus|1||
|Bas maskin avd. Gjøvik|Bas Maskinutleie avd. Gjøvik|1||
|Bas maskin AS avd Østfold|Bas Maskinutleie AS avd Østfold|1||
|Bas maskin avd. Elverum|Bas Maskinutleie avd. Elverum|1||
|Bas maskin Telemark Vestfold Buskerud|Bas Maskinutleie avd. Tvb (Telemark Vestfold Buskerud)|1||
|PRG maskin AS|PRG Maskinutleie AS|2||
|Ekspert maskin AS|Ekspert Maskinutleie AS|3||
|drammen Maskin Oslo|Drammen Maskin Utleie AS Avd Oslo|2||her er "maskin utleie" oppdelt|
|bas maskin Elverum|Bas Maskinutleie avd. Elverum|2||
|naboen maskin Stavanger|Naboen Utleie AS|2||Maskiner' i mco_text|
|kongsberg maskin Skollenborg|Kongsberg Lift og Maskinutleie AS|2||
|ekspert maskin Vøyenenga|Ekspert Maskinutleie AS|1||
|anlegg og maskin Isdalstø|Anlegg og Industriteknikk AS|1||
|maskin Stjørdal|Maskinutleie Stjørdal AS|10||
|odin maskin Kristiansund N|Odin Utleie|2||Gir treff i solgt søkeord|
|bas maskin Frogner|Bas Maskinutleie avd. Oslo/Akershus|2||
|bas maskin Hof|Bas Maskinutleie avd. Tvb (Telemark Vestfold Buskerud)|2||
|telemark maskin Notodden|Telemark Byggutleie TBU|10||Gir treff i solgt søkeord|
|persøy maskin Levanger|Persøy Lift & Maskinutleie AS|1||
|maxbo maskin Lier|Maxbo utleie (Toolmatic)|9999||Maskinutleie i bransje. Synonymet går ikkje til bransjen (enda hvertfall)|
|drammen maskin Drammen|Drammen Liftutleie AS|9999||Maskinutleie i bransje. Synonymet går ikkje til bransjen (enda hvertfall)|
|ekspert maskin Oslo|Ekspert Maskinutleie avd. Oslo|10||
|utleieprodukter maskin Kristiansand S|Utleieprodukter AS|9999||Maskinutleie i bransje. Synonymet går ikkje til bransjen (enda hvertfall)|
|akershus maskin Nannestad|Akershus Utleie AS|9999||Maskinutleie i bransje. Synonymet går ikkje til bransjen (enda hvertfall)|
|agledal maskinutleie|Agledal maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|agder maskinutleie, grimstad|Agder frø og maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Almås maskinutleie|Almås maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Brøndbo maskinutleie|Brøndbo maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Flatnes maskinutleie|Flatnes maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Øksnes maskinutleie|Øksnes maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Hillens maskinutleie|Hillens industri & maskin AB|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Levo mekanikk og maskinutleie |Levo maskin og mekanikk AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Strømmen maskinutleie|Strømmen maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|o mydland maskinutleie|O. Mydland maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Drugli maskinutleie, løkken verk|Drugli maskin og Transportservice|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Nedrebø maskinutleie, time|Nedrebø maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Kaspo maskinutleie, tungaveien 38|Kaspo maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Bjørn Nilsen maskinutleie, oteren|Bjørn Nilsen maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Elseth maskinutleie, torsbergveien porsgrunn|Elseth maskin AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|Målselv maskinutleie, rossvoll|Målselv maskin & transport AS|9999||Gir ikkje treff pga det ikke finnes synonym på at maskinutleie skal gi treff på maskin|
|communications drøbak|ILIOS Kommunikasjon AS|2||
|din dr sykepleiertjenester as|Din Doktor Sykepleietjenester AS|1||
|dr sypula|Doktor Sypula AS|1||
|dr mobil skedsmokorset|Doktor Mobil AS |1||
|bank olav femtes gate 6 olso|Handelsbanken Olav Vs gate|1||
|femtes rør oppegård|VS Rør AS|1||
|memira olav femtes gate stavanger|Memira avd. Stavanger |1||
|fjerdes arkitekter as|Kvarts Arkitekter AS|1||
|Kriminalomsorg i frihet kristiansand|Kriminalomsorgen Agder friomsorgskontor|3||
|legemiddelverket oslo|Statens legemiddelverk|1||
|nina, tungasletta|Norsk institutt for Naturforskning|1||
|sentralsykehuset stavanger|Sentralsjukehuset i Rogaland FKF|1||
|snekker tore lundarheim|Tore Lundarheim|1||
|sporten hvittingfoss|Sport1 Hvittingfoss AS|1||
|sport1 eidsvoll|Sporten Eidsvoll AS|1||
|olav femtes gate 6 stavanger|Vinmonopolet Stavanger, Straensenteret|1||
|øystese mekaniske verksted|Øystese Mek. Verkstad / MECA|1||denne har mekaniske også som eso|
|viggja mekaniske|Viggja Mek AS|1||
|støa mekaniske|Støa Mek AS|1||
|mekaniske verksted, melhus|Bjørgum Mek. Verksted AS|1||
|Lonbakken mek verksted|Lonbakken mekaniske verksted AS|1||
|songe mek verkstad|Songe mekaniske verksted AS|1||
|hokksund mek verksted|Hokksund mekaniske verksted AS |1||
|Hanstvedt Mek, osterøy|Hanstvedt Mekaniske Verksted|1||
|dagestad mek verksted|Dagestad mekaniske verksted AS|1||
|Lejre Mek Verksted|Lejre Mekaniske Verksted|1||
|løkka mek verksted|Løkka mekaniske verksted AS|1||
|mek verksted, skolmar 23|Løkka mekaniske verksted AS|1||
|mek verksted, sandefjord|Løkka mekaniske verksted AS|1||
|kommunelegen i naustdal|kommunelækjaren i Naustdal sunnfjord|1||
|kommunelegen eiken|kommunekækjaren i hægebostad|1||
|kommunelegen vang i valdres|kommunelækjaren i vang|1||

!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values |contain name?| actual values? |
|Sogndal lufthavn|Sogndal Lufthamn|Found||
|ahus|akershus universitetssykehus hf|Found||
|alf børve, arna|Alv Børve|Found||
|alv svein abrahamsen, kristiansand|Alf Svein Abrahamsen|Found||
|Hålogaland aviser|Hålogaland avis|Found||
|badstuer, sandnes|Ildsjelen Varmefag Sandnes|Found||
|western bankar, bergen|Western Union Bank|Found||
|drammens banken|DNB Bank ASA|Found||
|ølve barneskole|Ølve oppvekstsenter avd. skule|Found||Denne har ikkje skole/barneskole/ungdomsskule i bransje. Den har 'Offentlig forvaltning' Så denne viser at barneskole gir treff på skule og at det da er synonymet som slår inn|
|granvin barneskule|Granvin Barne- og ungdomsskule|Found||
|bodin begravelse|Bodin gravferd AS|Found||
|begravelsesbyrå vest|Gravferdsbyrået vest AS|Found||Finnes også bransje 'Begravelsesbyrå' på denna...|
|begravelsesbyråer tysnes|Tysnes og Sunnhordland Gravferdsbyrå|Found||Gravferdsbyrå er også søkeord til bransjen: begravelsesbyrå. Synonymene går om en annen..|
|nærøy bibliotek|Nærøy folkebibliotek|Found||Alle har bransje: bibliotek. Så denne er nok ikkje heilt reel..|
|bil med sjel|Biler med sjel AS|Found||Denne har ikkje bil/biler/bilsalg etc i bransje, men Forlag|
|barn i biler|If sikkerhetsbutikk Barn i bil|Found||
|biletekunstnarar, øystese|Sigbjørn Kvandal|Found||
|billedbyrå stavanger|Fotonova Bildebyrå|Found||
|vigrestad bo og velferdssenter|Vigrestad bu og velferdssenter|Found||
|laselv bredband|Finnmark bredbånd AS|Found||
|breiband Rossmollgata 50|Hammerfest Energi bredbånd AS|Found||
|brødr solem|Brødrene Solem AS|Found||
|brødrene langeland|Brødr Langeland A/S|Found||
|feviktun bu og omsorgssenter|Feviktun bo og omsorgssenter|Found||
|Cafeer, trondheim|Bangkok Cafe|Found||Tlf 73535600|
|Din Dr Sykepleietjenester|Din Doktor Sykepleietjenester AS|Found||
|norheimsund taxier|Norheimsund Øystese taxi AS|Found||
|dyreklinikk døgn senter, revetal|Dyreklinikk 24 døgnvakt - Dyrlegen senter|Found||
|Vestby folkebibliotek|Vestby bibliotek|Found||
|hm, oslo|H & M Hennes & Mauritz|Found||
