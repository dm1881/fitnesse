Kontaktpunkt med busmob1 gir treff på kontaktpunkt under enheten ved nummersøk

|${SEARCH_API} check top x name|
| query | expected |checkTop |result? |kommentar |
|90875325|Bergen Stål Bruktjern AS|1|||
|90887461|Bergen Stål Bruktjern AS|1|||
|69126619|Økonor Sarpsborg|1|||
|69126626|Økonor Sarpsborg|1|||
|69126627|Økonor Sarpsborg|1|||
|95159355|Dr. Oetker Norge AS|1|||
|32264742|Shell Oljesenter - Aass varme og Energi|1|||
|91396468|Nerlands Granittindustri AS|1|||
|40729225|Star Information systems AS|1|||
|57824045|Statoil butikk Førde|1|||
|90572993|Gig Networks AS|1|||
|51916630|Julia Thordis Svendsen Holtermann|1|||
|93264462|Comfort Stord Rørleggerforretning|1|||
|90297330|Energi nord AS|1|||
|47272089|Brattås total AS|1|||
|41564306|Fiskeri- og havbruksnæringens Forskningsfond|1|||
|90743903|SIFO - Statens institutt for forbruksforskning|1|||
|38334417|Å barneskole|1|||
|75012563|Brann- og Feiervesenet i Brønnøy|1|||
|97637410|Kynningsrud Prefab AS|1|||
|32088584|YES! Vi Leker|1|||
|98251448|Heimer AS|1|||
|98225242|Johnsen J & Sønner AS|1|||
|95266402|Klepp Energi|1|||
|90196060|Demas AS|1|||
|91367158|Bergen Caravan AS|1|||
|91367159|Bergen Caravan AS|1|||
|63007750|Anne line Strømness Paulsen|1|||
|41087830|Feltspatcompagniet AS|1|||
|95784540|Rybeltron AS|1|||
|93409783|Entreprenør Magnar Sivertsen AS|1|||
|91149267|G Løkken Autoverksted AS|1|||
|41509350|Bema AS|1|||
|93009579|Hwh Energimegling AS|1|||
|45422744|A Pedersen & sønn AS|1|||
|97689708|Go Plassen|1|||
|91746741|Østa elektro AS|1|||
|98211888|Byggmester Asbjørn Hitland AS|1|||
|92452356|Kirkens SOS i Agder|1|||
|48083671|Agder El installasjon Arendal AS|1|||
|40764286|Høvik AS|1|||
|55306359|Shape Consulting AS|1|||
|45234580|Metis education AS|1|||
|92462236|Økonomiservice Ringerike AS|1|||
|97057133|Nannestad Videregående Skole|1|||
|90727145|Vestskog BA|1|||
|46419445|X-partner nord AS avd. Alta|1|||
|45241766|Seadream Yacht Club Management AS|1|||
|92272421|Bil Mohagen|1|||
|92620447|Johs E Øvsthus (Byggeriet)|1|||
|91836200|Tec Con|1|||
|99352856|Defa Hyttami|1|||
|97791815|Defa Hyttami|1|||
|95250077|Advantec AS|1|||


SØK på nummer skal gi treff på kontaktpunkter med busmob3, Sjekk visningen i kanalene!

|${SEARCH_API} check top x name|
| query | expected |checkTop |result? |kommentar |
|92400003|REMA 1000 Skjold|1||Jarle Lilletvedt, Nesttun|
|69225047|Diplom-Is avd Rakkestad|1|||
|32893502|Vinmonopolet Slemmestad|1|||
|61109490|Fagteam Gjøvik, Bufetat, Region Øst|1|||
|75174629|Rema 1000 Nyrud|1|||
|75520391|Avinor Bodø lufthavn|1|||
|97641534|Inneklima Svein Oseng|1|||
|69376550|Den norske kirken Hvaler kirkelige fellesråd|1|||
|90042535|Martin's Agenturer AS|1|||
|76150072|Nordlandssykehuset Vesterålen|1|||
|70126439|Morris|1|||
|22048911|Det Kongelige Hoff|1|||
|99468551|Litlaperlå brasserie|1|||
|66772600|Holmen Fjordhotell|1|||
|33473919|Hvaltorvet Kjøpesenter|1|||
|37025472|Boots apotek Arendal|1|||
|52855283|Baretti|1|||
|90672507|Eura AS|1|||
|48990145|Hovedorganisasjonen Virke|1|||
|45257151|Sydvaranger Gruve AS|1|||
|75757406|Elkem Mårnes Kvartsittbrudd|1|||
|90557146|Kynningsrud Prefab AS|1|||
|63973086|Chantal Jessheim|1|||
|51891901|Specsavers Stavanger|1|||
|21393944|Antirustsenteret Kjell Hall|1|||
|90954306|Haug menighet|1|||
|92467980|Jarlsberg Trafikkskole AS|1|||
|40604903|Arna Jordsortering|1|||
|91547083|Fremover AS|1|||
|41423006|Gate Gourmet Norway AS|1|||
|69319839|Sjømannslege|1|||
|92606519|SINTEF Energi AS|1|||
|23120638|Regus business Centre Avd. Skøyen|1|||
|56333697|Burger King|1|||
|67144871|Eiksmarka bilpleie YX Grini|1|||
|92815571|Finstad Eivind|1|||
|97599360|Aspelin-Ramm eiendom AS|1|||
|95441277|Agathon Borgen AS|1|||
|51483344|Advokatfirmaet Lima|1|||
|33334443|Fingerbøla AS|1|||
|98284503|Uni pluss AS|1|||
|48400421|Ringve Museum|1|||
|92236412|ITC Intermec technologies Corporation AS|1|||
|38263818|Afrodite Stormote|1|||
|99297898|Cam Cam Hair team|1|||
|52731227|BR-Leker|1|||
|92202182|Vera & William|1|||
|41515500|Øivind Hviding elektro|1|||
|75023370|JYSK Brønnøysund|1|||
|95757736|Rosenberg Worleyparsons AS|1|||
|97787708|Vakt service AS - Telemetri|1|||
|85424901|Satpos AS|1|||
|90505348|Rælingen videregående skole|1|||
|78463599|Porsanger kommune|1|||
|75012552|Brann- og Feiervesenet i Brønnøy|1|||
