|${SEARCH_API} check top x name|
|query|expected|checkTop|result?|kommentar|
|Inger Nordby, TRONDHEIM|Inger Lund Nordby Trondheim|1|||
|Stangeland maskin, SOLA|Stangeland Maskin AS Sola|1|||
|AS Odd Fellow-Huset, GRIMSTAD|Odd Fellow Huset AS Grimstad|1|||
|Margrethe Usterud-Svendsen, TOLVSRØD|Margrethe Usterud-Svendsen Skallestad|1|||
|Alf Årethun, SOGNDAL|Alf Aarethun Lærdal|1|||
|Janne's tekstil AS, STAVERN|Jannes tekstil AS Stavern|1|||
|Leif Trygve Jørgensen, KRISTIANSAND S|Leif Trygve Jørgensen Kristiansand S|1|||
|Smurfit Kappa Rena AS, BODØ|Smurfit Kappa Rena AS Bodø|1|||
|Rutebåten Utsira, UTSIRA|Rutebåten Utsira|1|||
|RANA Næringsforeining, MO I RANA|Rana Næringsforening Mo i Rana|1|||
|Bjørn Halstvedt, SKIEN|Bjørn Halstvedt Skien|1|||
|Rapid GRUPPEN, AURSKOG|Rapid gruppen AS Aurskog|1|||
|Elisabet NORMANN, Tvedestrand|Elisabeth Normann Tvedestrand|1|||
|Astrid Faret, SANDNES|Astrid Faret Sandnes|1|||
|Iselin Bjerke Vatle, SPYDEBERG|Not found(searchresult empty)|1|||
|Katrine Myrmehr, FYLLINGSDALEN|Cathrine Myrmehl Fyllingsdalen|1|||
|Rett Frem Transport AS, OSLO|Rett Frem Transport AS Oslo|1|||
|Hillevåg EIENDOM AS, STAVANGER|Hillevåg Eiendom AS Stavanger|1|||
|Garmin Norge AS, DILLING|Garmin Norge AS Dilling|1|||
|Voss Kjøttindustri AS, VOSSESTRAND|Voss Kjøttindustri AS Vossestrand|1|||
|Hemne Næringsforum, KYRKSÆTERØRA|Hemne Næringsforum Kyrksæterøra|1|||
|Safe, STAVANGER|Safe Stavanger|1|||
|OSLO Flaggfabrikk AS, MOSS|Oslo Flaggfabrikk AS Dilling|1|||
|Tromas AS, TRONDHEIM|Tromas AS Trondheim|1|||
|Hegglandsdalen Barnehagelag, OS|Hegglandsdalen Barnehagelag SA OS|1|||
|Ikomm AS, LILLEHAMMER|Ikomm AS Lillehammer|1|||
|Kovent as, BJERKREIM|Covent AS Bjerkreim|1|||
|ONARHEIM eiendom LØRENSKOG|Onarheim eiendom A/S Lørenskog|1|||
|Langesund Sjømat og Fiskefestival, LANGESUND|Not found(searchresult empty)|1|||
|UPPER EAST OSLO|Upper east oslo|1|||
|LHL, lillehammer|LHL Lillehammer Lillehammer |1|||
|Lærdal Energi AS, LÆRDAL|Lærdal Energi AS Lærdal|1|||
|Hammerfest Industriservice AS, HAMMERFEST|Hammerfest Industriservice AS Hammerfest|1|||
|Studio G AS, HEIMDAL|Studio G fotografene AS Heimdal|1|||
|Blikk OG takservice, JAREN|Blikk og Takservice AS Jaren|1|||
|Kaare haugsten, FREDRIKSTAD|Kåre Haugsten Gamle fredrikstad|1|||
|norges GEOTEKNISKE institutt, OSLO|NGI Norges Geotekniske Institutt oslo|1|||
|KORSMO BARNEHAGE KONGSVINGER|Korsmo barnehage Skarnes|1|||
|INK TATTOO ålesund|ink spot tattoo Ålesund|1|||
|Hilde Hådem, TRONDHEIM|Hilde Hådem Trondheim|1|||
|Vestlandske AUTO as, ÅLESUND|Vestlandske Auto AS Ålesund|1|||
|Mix Farsund, FARSUND|Mix Farsund Farsund|1|||
|Data Pro AS, ÅLESUND|Data Pro AS Ålesund|1|||
|Vang Sparebank, VANG I VALDRES|Vang Sparebank vang i valdres|1|||
|teco Maritim, PORSGRUNN|TECO Maritime Norway AS Porsgrunn|1|||
|Gulvfabrikken AS, FORNEBU|Gulvfabrikken AS Fornebu|1|||
|Hanne Iveland Henriksbø, OSLO|Hanne Iveland Henriksbø Oslo|1|||
|Ålhytta AS, ÅL|Ålhytta AS ål|1|||
|Sveio Blomster og Gaver AS, SVEIO|Sveio Blomster og Gaver AS Sveio|1|||
|Stein SØRENSEN Persontransport Din Taxi AS, TROMSØ|Stein Sørensen Persontransport AS Tromsø|1|||
|Pm Entreprenør AS, YTRE ENEBAKK|Pm Entreprenør AS Ytre enebakk|1|||
|Ingvald Husabø Prenteverk AS, LEIKANGER|Ingvald Husabø Prenteverk AS Leikanger|1|||
|Sikke, BARDU|Sikke blomster & interiør AS Bardu|1|||
|Otralaft AS, KRISTIANSAND S|Otralaft AS Kristiansand S|1|||
|Tynset Diesel AS, TYNSET|Tynset Diesel AS Tynset|1|||
|Lege Ørjan Vågane, BRATTHOLMEN|Ørjan Henning Vågane Brattholmen|1|||
|Pan Renhold & Kantine, STAVANGER|Pan Renhold & Kantine Stavanger|1|||
|ingval dahl, VINSTRA|Ingvald Dahl Vinstra|1|||
|Toyota Førde AS, FØRDE|Toyota Førde AS Førde|1|||
|Osland Havbruk AS, BJORDAL|Osland Havbruk AS Bjordal|1|||
|Allegro Finans ASA, TRONDHEIM|Allegro Finans ASA Trondheim|1|||
|E & A Eide AS, BERGEN|Eide Taksering Bergen|1|||
|Hallingplast AS, ÅL|Hallingplast AS Ål|1|||
|Tor O Næss Butikkinredning AS, FREDRIKSTAD|Næss Butikkinredning AS Tor O. Fredrikstad|1|||
|Ac Teknikk Jaren AS, JAREN|Ac Teknikk Jaren AS|1|||
|Flekkefjord Glassmagasin AS, FLEKKEFJORD|Flekkefjord Glassmagasin AS Flekkefjord|1|||
|Gourmet Catering, SKI|J & P Gourmet catering Ski|1|||
|Jostein Vorland, SKOGSVÅG|Jostein Vorland Skogsvåg|1|||
|Arnold Klingsheim, AKSDAL|Arnold Klingsheim Aksdal|1|||
|Anett Lüdecke, HARSTAD|Anett Lüdecke harstad|1|||
|Adrian Lüdecke Naturmaling, OSLO|Adrian Ludecke Naturmaling Oslo|1|||
|Karine Skinner, STABEKK|Karine skinner Stabekk|1|||
|Norsk Cater AS, KONGSVINGER|Not found(searchresult empty)|1|||
|Ecoonline AS, TØNSBERG|Ecoonline AS Tønsberg|1|||
|Sindre Leganger, OSLO|Norsk Rikskringkasting NRK Oslo|1||Norsk Rikskringkasting NRK er riktig, Sindre Leganger kommer opp som ansatt (har busmob 1)|
|Uponor AS, VESTBY|Uponor AS Vestby|1|||
|Natur Videregående Skole, OSLO|Natur Videregående Skole Oslo|1|||
|Systempartner Norge AS, SANDEFJORD|Systempartner Norge AS Sandefjord|1|||
|Sletten Norge AS, OSLO|Sletten Norge AS Oslo|1|||
|Hammerfest Boligbyggelag, HAMMERFEST|Hammerfest og omegn boligbyggelag - garanti eiendomsmegling Hammerfest|1|||
|Scandec Systemer AS, TROLLÅSEN|Scandec Systemer AS Trollåsen|1|||
|Rotor Promotion AS, KLÆBU|Not found(searchresult empty)|1|||
|Toves Hårdesign AS, LAKSELV|Toves Hårdesign AS Lakselv|2|||
|Kystlab AS, MOLDE|Kystlab AS Molde|1|||
|Umoe offshore KRISTIANSUND N|Umoe sterkoder AS Kristiansund N|1|||
|dargan drammen|Dargan AS Mjøndalen|1|||
|Only  STAVANGER|Only Stavanger|1|||
|karl berner Kjelleren, OSLO|Carl Berner Kjellern AS Oslo|1|||
|Johan B Ukkestad AS, SKARNES|Johan B Ukkestad AS Skarnes|1|||
|modelljernbane import HORTEN|Vestfold modelljernbane import Horten|1|||
|Nord-Norsk Aluminium AS, TOVIK|Nord-Norsk Aluminium AS Tovik|1|||
|Salgvest AS, STAVANGER|Salgvest AS Stavanger|1|||
|Boen Bruk AS, TVEIT|Boen Bruk AS Tveit|1|||
|Oslo Patentkontor AS, OSLO|Oslo Patentkontor AS Oslo|1|||
|Telemark Entreprenørservice AS, PORSGRUNN|Telemark Entreprenørservice AS Porsgrunn|1|||
|Fjordtønna Da, TYSSE|Fjordtønna Da Tysse|1|||
|Fremtiden Eiendomsmegling AS, OSLO|Aktiv Grønland / Bjørvika Oslo|1|||
|CONTENDO, LYSAKER|Contendo AS Lysaker|1|||
|Hafs Elektro AS, LEIRVIK I SOGN|HAFS Elektro AS (Elfag) Leirvik i Sogn|2|||
|telenor kundeservice, NOTODDEN|Telenor Mobil Kundeservice Landsdekkende|1|||
|Empack Nor AS, OSLO|Empack Nor AS Oslo|1|||
|Hardanger Fjellfisk AS, TYSSEDAL|Hardanger Fjellfisk tyssedal|1|||
|Markup Consulting AS, OSLO|Markup Consulting AS Oslo|1|||
|Skaget Betongsaging AS, TRONDHEIM|Skaget Betongsaging AS Trondheim|1|||
|AS Oppland Metall, HUNNDALEN|AS Oppland Metall Hunndalen|1|||
|Mye I Media AS, MO I RANA|Mye I Media AS Mo i Rana|1|||
|Resmed Norway AS, HØVIK|Resmed Norway AS Høvik|1|||
|Strømstad Elektro AS, KVITESEID|Strømstad Elektro AS Kviteseid|1|||
|Hagens Karosseriverksted, KRØDEREN|Hagens Bil og Karosseriverksted (Bosch Car Service) Krøderen|1|||
|Fotograf Gina Englund AS, LANGHUS|Fotograf Gina Englund AS Langhus|1|||
|Einar Stange AS, OSLO|Einar Stange AS Oslo|1|||
|Bolt Reklamebyrå AS, HVALSTAD|Bolt Reklamebyrå AS Hvalstad|1|||
|Kruse Topptur AS, HEMSEDAL|Kruse Topptur Hemsedal |1|||
|glassmester Dag Kvamme, ALMENNINGEN|Glassmester Kvamme AS Almenningen|1|||
|Briz Norge, TRONDHEIM|Briz Trondheim|1|||
|Vikeså Glassindustri AS, BJERKREIM|Vikeså Glassindustri AS Bjerkreim|1|||
|Alt I Asfalt AS, GJERDRUM|Alt I Asfalt AS Gjerdrum|1|||
|Sanda Camping og Hytteutleie AS, BØ I TELEMARK|Sanda Camping og Hytteutleie AS Bø i Telemark|1|||
|Therese Dalen, TYNSET|Therese Dalen Tynset|1|||
|Safety Design AS, KRISTIANSAND S|Safety Design AS Kristiansand S|1|||
|Olav Imset Lien, HEIMDAL|Olav Imset Lien Heimdal|1|||
|Autotech Oslo Ltd, OSLO|Autotech Oslo Ltd Oslo|1|||
|Eminent Eiendom AS, HOVDEN I SETESDAL|Eminent Eiendom AS Hovden i Setesdal|1|||
|Eidsmo Bruk, HØYLANDET|Eidsmo Bruk Geir Joar Werstad Høylandet|1|||
|Myklebust Kunst & Handverk AS, VASSENDEN|Myklebust Kunst & Handverk AS Vassenden|1|||
|Stanges Gårdsprodukter AS, TØNSBERG|Stanges Gårdsprodukter AS Tønsberg|1|||
|Juvelen Norge AS, ØSTERÅS|Juvelen Trekanten Asker|1|||
|Quality Security AS, FREDRIKSTAD|Quality Security AS Kråkerøy|1|||
|Rauland Høgfjellshotell, RAULAND|Rauland Høgfjellshotell AS Rauland|1|||
|Egersund Pizza AS, EGERSUND|Egersund Pizza AS Egersund|1|||
|Anne Berit Selle, HYEN|Anne Berit Selle Hyen|1|||
|Orbit Gmt AS, FØRRESFJORDEN|Orbit GMT Førresfjorden|1|||
|Svein Ludvigsen, TROMSDALEN|Svein Ludvigsen Tromsdalen|1|||
|Arne Binde, VIKHAMMER|Arne Håkon Binde Vikhammer|1|||
|Sigyn Melvær Sunde, FØRDE|Sigyn Melvær Sunde Førde|1|||
|Kristian Aasen, LILLESTRØM|Kristian Andreas Aasen Lillestrøm|1|||
|Liv Marit Amundsen, HARESTUA|Liv Marit Amundsen Harestua|1|||
|Sigbjørn Hansen, LUNDENES|Sigbjørn Elias Hansen Lundenes|1|||
|Sturle Valla, MO I RANA|Sturle Kristian Valla Mo i rana|1|||
|Anne Berit Alme, DEGERNES|Anne Berit Alme Degernes|1|||
|john bakke, GIMSE|John Allan Bakke Gimse|1|||
|Geir Hammer, GRINDER|Geir Jonny Hammer Grinder|1|||
|Kjell Ellingsen, BODØ|Kjell Ellingsen Bodø|1|||
|Frank Paulsen, HUNDVÅG|Frank Gunnar Paulsen Hundvåg|1|||
|Randi Wold, BREKSTAD|Randi Marita Lund Wold Brekstad|1|||
|Finn ulfberg, KONGSVINGER|Finn Ulf Berg Kongsvinger|1|||
|Alessandro Dabeed, OSLO|Alejandro Elias Dabed Diaz Oslo|1|||
|Olaf Korbu, KONGSBERG|Olaf Korbu Kongsberg|1|||
|Leif Grutle, VALEVÅG|Leif Grutle Valevåg|1|||
|Vigdis Helene Bikset, TJØTTA|Vigdis Helene Bikset Tjøtta|1|||
|Ada Pettersen, HARSTAD|Ada Helga Pettersen Harstad|2|||
|May Liss Thomassen, ALTA|May Liss Thomassen Alta|1|||
|Halvor Gramme, HASLUM|Halvor Gramme Haslum|1|||
|jorge monsalve, OSLO|Jorge Schultzky Monsalve Oslo|1|||
|marianne nesse, STRUSSHAMN|marianne nesse Strusshamn|1|||
|herland garmo, OSLO|Erland Garmo Oslo|1|||
|Magne Skadsheim, MYSEN|Magne Skadsheim Mysen|1|||
|Roald Kristoffersen, JESSHEIM|Roald Kristoffersen Jessheim|1|||
|Trond Jacob Teigen, ASKER|Trond Jacob Teigen Asker|1|||
|Monica Søvde, GREÅKER|Monica Therese Søvde Greåker|1|||
|Anne Britt Rønning, AVERØY|Anne Britt Rønning Averøy|1|||
|Gry Ottem, SANDEFJORD|Gry Ottem Sandefjord|1|||
|Helge Grøstad Fløisand, LODDEFJORD|Helge Grøstad Fløisand Loddefjord|1|||
|Helge Aker, LARKOLLEN|Helge Aker Larkollen|1|||
|Jan Erik Dahl, LARVIK|Jan Erik Dahl Larvik|1|||
|Helge Tolleshaug, OSLO|Helge Tolleshaug Oslo|1|||
|Anne Grete Omdaal, EGERSUND|Anne Grete Stenberg Omdal Egersund |1|||
|Anne Gret Håauge, BYGLAND|Not found(searchresult empty)|1|||
|Gunn Dyrdal, RÅDAL|Gunn Hilde Dyrdal Rådal|1|||
|Astrid Stødle, ARNATVEIT|Astrid Stødle Arnatveit|1|||
|Kristine Lundekvam, KOLLTVEIT|Kristine Lundekvam Kolltveit|1|||
|Odd Barstad, OSLO|Odd Barstad Oslo|1|||
|INGVILD LEIVESTAD OSLO|Ingvild leivestad oslo|1|||
|SVEIN SOGNES, brekke|Svein Sognnes Brekke|1|||
|May Britt Eibakk, ASKIM|May-Britt Eibak Askim|1|||
|Britt Larsen, SKJEBERG|Britt Mari Smerkerud Larsen Skjeberg|1|||
|Siv Borgersen, STAVANGER|Siv Kirstin Thomassen Borgersen Stavanger|2|||
|gerd walberg, SKJEBERG|gerd walberg Skjeberg|1|||
|REIDAR HAGEN, STRØMMEN|Reidar Hagen Strømmen|1|||
|Ragni Bjelke Sandberg, OSLO|Ragni Bjelke Sandberg Oslo|1|||
|Elisabeth Moe, DRAMMEN|Elisabeth Dysthe Moe Drammen|1|||
|mette sannes, MOSS|Mette Synnøve Sannes Moss|1|||
|gerd nesbø, FØRDE|gerd nesbø Førde|1|||
|Anna Kvitberg, TØNSBERG|Anna Kvitberg Tønsberg|1|||
|Oddvar Wæhler, OSLO|Oddvar Wæhler Oslo|1|||
|Gretha Pettersen, FANA|Gretha Rolfsvaag Pettersen Fana|1|||
|Kjetil Landsverk, KVITESEID|Kjetil Landsverk Kviteseid|1|||
|Kjell skjelvik, OPPHAUG|Kjell Johan Skjelvik opphaug|1|||
|Signe Sørvig, KRISTIANSAND S|Signe Føreland Sørvig Kristiansand S|1|||
|Senada Sehovic, KRISTIANSAND S|Senada Sehovic Kristiansand S|1|||
|Hanne Marie Myrvoll, OSLO|Hanne Marie Myrvold Oslo|1|||
|Kristin Hagedal, KRISTIANSUND N|Kristin Hagedal Kristiansund N|1|||
|Ingolf Petersen, INDERØY|Ingolf Zeiner Petersen Levanger|1|||
|Marie Stenseth, BODØ|Marie Fygle Stenseth Bodø|1|||
