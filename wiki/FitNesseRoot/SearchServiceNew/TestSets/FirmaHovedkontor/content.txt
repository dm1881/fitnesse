Her er ein del som feiler. Kan være fleire som bør få eso, flagg eller SearchBB.

!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values | contain ranked name? | actual values? |
| AA hovedkontor oslo | Stiftelsen Anonyme Alkoholikere i Norge | Found ranked | |
| Adecco hovedkontoret oslo | Adecco Norge AS | Found ranked | |
| arts and crafts hovedkontor | Arts & Crafts AS | Found ranked | | Gir 0-treff. Søker en 'arts & crafts hovedkontor' så gir dette treff. |
| arts & crafts hovedkontor | Arts & Crafts AS | Found ranked | |
| Black Design | Black Design Shop AS | Found ranked | |
| chess hovedkontor | OneCall | Found ranked | | Treff på historisk navn |
| Color line hovedkontor | Color Line AS | Found ranked | |
| mcdonalds hovedkontor | McDonald's Norge AS | Found ranked | |
| Coop hovedkontor | Coop Norge handel - Hovedkontor | Found ranked | | tlf 22899595 |
| curato hovedkontor | Curato Røntgen AS (Administrasjon) | Found ranked | | Er lagt inn eso |
| Dekkmann hovedkontor | Dekkmann AS | Found ranked | | Tlf 23068100 |
| DnB hovedkontor | DNB Bank ASA | Found ranked | |
| Dolly Dimples hovedkontor | Dolly Dimple's Norge AS | Found ranked | | Apostroff som ikkje fungerer |
| Dolly Dimple's hovedkontor | Dolly Dimple's Norge AS | Found ranked | |
| Elixia hovedkontor | Hfn Norway AS | Found ranked | |
| elkjøp hovedkontor | Elkjøp Nordic | Found ranked | |
| europris hovedkontor | Europris AS | Found ranked | |
| Expert hovedkontor | Power Norge AS | Found ranked | | Navnendring, lagt inn i SBB |
| felleskjøpet hovedkontor | Felleskjøpet AGRI Hovedkontoret | Found ranked | |
| Fjellinjen hovedkontor | Fjellinjen AS | Found ranked | |
| Fjordkraft hovedkontor | Fjordkraft AS | Found ranked | | Folke bernadottes vei 38, fyllingsdalen |
| get hovedkontor | Get AS | Found ranked | |
| Gjensidige forsikring hovedkontor | Gjensidige Forsikring | Found ranked | |
| Gjensidige hovedkontor | Gjensidige Forsikring | Found ranked | |
| H&M hovedkontor | H & M Hennes & Mauritz AS | Found ranked | |
| Hafslund hovedkontor | Hafslund Nett AS | Found ranked | |
| hennes og mauritz hovedkontor | H & M Hennes & Mauritz AS | Found ranked | |
| hovedkontor eurest | Compass group Norge | Found ranked | |
| Hovedkontor Widerø | Widerøe's Flyveselskap AS | Found ranked | |
| ICA HOVEDKONTOR OSLO | ICA Norge AS | Found ranked | |
| If forsikring hovedkontor | IF Skadeforsikring | Found ranked | |
| jula hovedkontor | Jula Norge AS | Found ranked | |
| kredinor hovedkontor | Kredinor | Found ranked | |
| Lefdal hovedkontor | Lefdal Elektromarked AS | Found ranked | |
| lindex hovedkontor | Lindex AS | Found ranked | |
| Lindorf hovedkontor | Lindorff AS | Found ranked | |
| Living hovedkontor | Living AS | Found ranked | |
| Lyse energi hovedkontor | Lyse Energi AS | Found ranked | |
| Lånekassen hovedkontor | Statens Lånekasse for utdanning | Found ranked | |
| MAXBO HOVEDKONTOR | Løvenskiold Handel kjedekontor for MAXBO | Found ranked | |
| Meny hovedkontor | Meny AS | Found ranked | |
| Miele hovedkontor | Miele AS | Found ranked | |
| Naf hovedkontor | NAF Norges Automobil-Forbund | Found ranked | |
| narvesen hovedkontor | Narvesen Norge | Found ranked | |
| NAV hovedkontor Oslo | Arbeids- og velferdsdirektoratet | Found ranked | |
| Netcom hovedkontor | Netcom AS | Found ranked | |
| Nettbuss hovedkontor | Nettbuss AS | Found ranked | |
| Nextgentel hovedkontor | NextGenTel AS | Found ranked | |
| Nille hovedkontor | Nille AS | Found ranked | |
| Nordea hovedkontor | Nordea Bank Norge ASA | Found ranked | |
| Norwegian hovedkontor | Norwegian ASA | Found ranked | |
| NSB hovedkontor | Norges Statsbaner AS | Found ranked | |
| One Call hovedkontor | One Call | Found ranked | |
| Oslo taxi hovedkontor | Oslo Taxi AS | Found ranked | |
| Peppes pizza hovedkontor | Peppes Pizza Hovedkontor | Found ranked | |
| posten hovedkontor | Posten | Found ranked | |
| Rema 1000 hovedkontor | Rema 1000 Norge AS | Found ranked | |
| rikstv hovedkontor | RiksTV | Found ranked | |
| santander hovedkontor | Santander Consumer Bank AS | Found ranked | |
| SAS hovedkontor | SAS Scandinavian Airlines Norge AS | Found ranked | |
| Sats hovedkontor | SATS Norge AS | Found ranked | |
| Shell hovedkontor | AS Norske Shell | Found ranked | |
| Skagerak energi | Skagerak Energi AS | Found ranked | |
| skatteetaten hovedkontor | Skattedirektoratet | Found ranked | |
| Skeidar hovedkontor | Skeidar AS | Found ranked | |
| Spar hovedkontor | Spar Norge AS | Found ranked | |
| Sparebank 1 oslo hovedkontor | SpareBank 1 Oslo | Found ranked | | Gir ikkje treff på SpareBank 1 Oslo med mco_text1 Hovedkontor.100124868S1 |
| Sparebanken sør hovedkontor | Sparebanken Sør Arendal | Found ranked | |
| Sparebanken vest hovedkontor | Sparebanken Vest | Found ranked | |
| startour hovedkontor | Star Tour | Found ranked | |
| statens kartverk | Statens kartverk | Found ranked | |
| Statens vegvesen hovedkontor | Statens vegvesen ¤ Vegdirektoratet | Found ranked | |
| statoil hovedkontor | Statoil hovedkontor | Found ranked | |
| Statoil hovedkontor stavanger | Statoil hovedkontor | Found ranked | |
| Talkmore hovedkontor | TalkMore AS | Found ranked | |
| Tele2 hovedkontor | Tele2 Norge AS | Found ranked | |
| Telenor Hovedkontor | Telenor ASA | Found ranked | |
| Tine hovedkontor oslo | TINE SA | Found ranked | |
| Tromsø taxi hovedkontor | Tromsø Taxi AS | Found ranked | |
| Tryg forsikring hovedkontor | Tryg forsikring | Found ranked | |
| TV2 hovedkontor | TV 2 | Found ranked | |
| ventelo hovedkontor | Phonero Kristiansand | Found ranked | | Ventelo er nå Phonero |
| viasat hovedkontor | Viasat AS | Found ranked | |
| volvat hovedkontor | volvat medisinske senter AS | Found ranked | |
| Widerøe hovedkontor | Widerøe's Flyveselskap AS | Found ranked | |
