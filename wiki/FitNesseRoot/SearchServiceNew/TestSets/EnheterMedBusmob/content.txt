Kontaktpunkt med busmob1 gir treff på kontaktpunkt i enheten dersom det er forskjeller i navn på kontaktpunkt vs. enhetsnavn. Verifiser at kontaktpunktet vises i frontend (det kommer ikke frem her)

!|I want to test panorama |
| query | expected values |contain name?| actual values? |
|ina marie tufte, eidsvåg|SGS Norge AS|Found||
|odd joar berg, molde|Solid Engineering AS|Found||
|jens sletten nesttun|Nico Elektro AS|Found||
|Christine Ormehaug|Nico Elektro AS|Found||
|anikken olsen, strømmen|Stiftelsen Akershusmuseet|Found||
|Geir Ove Molvær, Ulsteinvik|Mare Safety AS|Found||
|Fred Horghagen, Tiller|SINTEF NBL AS|Found||
|Endre Brennum, Lillehammer|Litra AS|Found||
|Morten Pihl, Oslo|Hyundai motor Norway AS|Found||
|Ketil Hofstad, Moss|Handicare AS|Found||
|Kjetil Holen, Moss|Handicare AS|Found||
|Amalie Friden, Nodeland|Master Norge AS|Found||
|Selger Seks, Bergen|7-Fjell Kapital AS|Found||
|Steinar Igelkjøn kontor, bergen|7-Fjell Kapital AS|Found||
|Lars-Erik Engebretsen, Gressvik|Edelweiss AS|Found||
|Per Lasse Bøckmann, gressvik|Edelweiss AS|Found||
|Dorthe-Kamilla Krogsrud, Kongsvinger|Økonor Kongsvinger|Found||
|Asbjørn Dale, stord|Klima og Energi Service AS|Found||
|Einar Johan Berge, Sirevåg|Sir Fish AS|Found||
|Gudmund Pettersen Tromsø|Kongsberg Spacetec AS|Found||
|Anders Thorstad Trondheim|Direktoratet for arbeidstilsynet|Found||
|Toril Berge, Trondheim|Direktoratet for arbeidstilsynet|Found||
|Frank Jenssen Trondheim|Direktoratet for arbeidstilsynet|Found||
|Tove Kristiansen Trondheim|Direktoratet for arbeidstilsynet|Found||
|Rosmari Johnsen Trondheim|Direktoratet for arbeidstilsynet|Found||
|Harald Skaar Ålesund|Ocean Supreme AS|Found||
|Vepsebolet, Kopervik|Avd Revehiet|Found||
|Morten Ulriksen Lysaker|Xerox AS|Found||
|Anne line Ullerlien, Skarnes|Trans AS|Found||
|Martine Trosterud Skarnes|Trans AS|Found||
|Kay Svendsen Tranby|Schneidler Norge AS|Found||
|Frode Ingvoldstad nesbru|Sportsmaster|Found||
|Bente Morland Rypefjord|Polarbase AS|Found||
|Marianne Iversen Sandefjord|Plenum Sandefjord AS|Found||
|Hans Teien Heimdal|B Skotvoll AS|Found||
|Ole Engan Trondheim|Maintech AS|Found||
|Inger-Johanne Frogh Kongsberg|Kongsberg kirkelige fellesråd|Found||
|Jostein Frode Olsen Hokksund|H-Vinduet Fjerdingstad AS|Found||
|Bjørn Erik Thorbjørnsen Moss|Meglergaarden AS|Found||
|Kjell Norli Sarpsborg|Servicesenteret Østfold AS|Found||
|Peter Gruttner Ski|Interiørverkstedet AS|Found||
|Geir Behn Lier|Lier Everk|Found||
|Morten Øren Lier|Lier Everk|Found||
|Øyvind Edvardsen, Hamar|Stiftelsen det norske Skogfrøverk|Found||
|Sofie Toft Kristiansand|HTH kjøkken Kristiansand AS|Found||
|Roger Martinussen Tromsø|Aurora kino IKS|Found||
|Rolf Fogderud Hokksund|Bråten Bilco AS|Found||
|Thomas Thomasse, Trondheim|Arc arkitekter AS|Found||
|Christian Hofmeier, Trondheim|Arc arkitekter AS|Found||
|Ole-Petter Ekornrud, Bekkestua|Takstsenteret AS|Found||
|Ole Egil Reitan, Trondheim|Norsk Marinteknisk Forskningsinstitutt MARINTEK|Found||
|Evgeniya Humberseth, Ørsta|Humberseth Reinhald AS|Found||
|Erling Hetland, Haugesund|Deltapump AS|Found||
|Erik Lehmann, Vestby|Strandco AS|Found||
|Magdalena Agata Oud, Hønefoss|Byggmakker Hønefoss|Found||
|Nils Kristian Høiseth Svarstad|Gjerden Fjellsikring AS|Found||
|Rune Almås Tananger|NorSea AS|Found||
|Sindre Dammerud Måløy|Sætren installasjon AS|Found||
|Rune Berg Måløy|Sætren installasjon AS|Found||
|Handyman Reserve Måløy|Sætren installasjon AS|Found||
|Rudland dagligvare, Kvam|KIWI Kvam|Found||
|Inge Forseth Stjørdal|Glen Dimplex nordic AS|Found||
|Thorstensen eftf AS El Installatør|Teft installasjon AS|Found||
|Hotel Amanda AS|Clarion Collection Hotel Amanda (Choice)|Found||
|Gausdal kommune|Gausdal ungdomsskole|Found||
|Åsnes kirkelige fellesråd|Åsnes Kirke|Found||
|øygarden kommune|Tjeldstø barnehage|Found||
|Jon myrseth|Comperio AS|Found||
|Jan anders sørheim, stord|Advantec AS|Found||
|Magnus Lindwall|Advantec AS|Found||
|Magnus Lindvald|Advantec AS|Found||
|Sissel Johnsrud Oslo|Hofstads AS|Found||
|Alexander Kvernstad Trondheim|Nylander & Partners Nybygg|Found||
|Pål Schei, Trondheim|Nylander & Partners Nybygg|Found||
|Jahn Dalheim Trondheim|Nylander & Partners Nybygg|Found||

!|I want to test panorama |
| query | expected values |contain ranked name?| actual values? |
|kurt driftland|Bringedal elektro AS|Found ranked||

Kontaktpunkt med busmob 3 gir ikkje treff på kontaktpunkt i enheten sjølv om det er forskjeller i navn på kontaktpunkt vs. enhetsnavn. Verifiser at kontaktpunktet IKKE vises i frontend (det kommer ikke frem her)

!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values |contain name?| actual values? |
|Øystein Støle, Eidsvåg i åsane|SGS Norge AS|Not found||
|Lillian Iolanda Salte, Eidsvåg i åsane|SGS Norge AS|Not found||
|Jarle Lilletvedt, Nesttun|Skjold dagligvarer AS|Not found||
|Rune Reinli|E Halvorsen elektro AS|Not found||
|Jone Heggheim, Stavanger|Svithun finans AS|Not found||
|Elisabeth Lein, oslo|Beths Beauty Center AS|Not found||
|Martin Steen, OS|Dekk1 Os|Not found||
|Sven Borgen, Gardermoen|Sibonor AS|Not found||
|Fredrik Viste Olsen, Hafrsfjord|Olsen advokat Geir Magne|Not found||
|Gry Elisabeth Bjørkan, Tønsberg|Familievernkontoret i Vestfold (Bufetat region sør)|Not found||
|Jorunn Tunhheim Røynestad, Stavanger|Rogaland Elektro Telecom AS|Not found||
|Stein Gellein, Moss|Lönne elektromotor & generatorservice AS|Not found||
|Elisabeth Fjeldheim Henriksen, Vøyenenga|Print Supplies AS|Not found||
|Bjørn Ole Øien, Oslo|Leithe & Christiansen AS|Not found||
|Lars Gunnar Andersson, Bergen|Univern Senter Bergen AS|Not found||
|Axel Bakke, Tjodalyng|Byggmesterfirma Jørgensen & Kemkers AS|Not found||
|Voice Norge, ski|Vic Ski|Not found||
|Per Kr Fossum S, Enebakk|Fossum Snø og anlegg Enebakk|Not found||
|Camilla Jakobsen|PVS Bergen|Not found||
|Kunde Ida, Ski|Salong Edmund|Not found||
|Jon Rokseth, Heimdal|Fretex Midt-Norge AS|Not found||
|Randi Marie Buaas, Stjørdal|Stokmoen fysioterapi DA|Not found||
|Tommy Skarvik, Rognan|Oseng Svein|Not found||
|Andre Storstein, Stjørdal|Høy Puls|Not found||
|Vibeke Arntzen, steinkjer|Teknor system AS|Not found||
|Ann Louise Brurås, Paradis|Wahwah SKIN Trim studio AS|Not found||
|Ragnhild Østerbø, Nesttun|Valid Forvaltning AS|Not found||
|Trondheim kommune, Tiller|Tonstad skole|Not found||
|Ipad, Slependen|Taberna Ramme AS|Not found||
|Silje Narum, Gjøvik|Quality Hotel Strand|Not found||
|Terje Overgård, Oslo|Opplæringssenteret for visuell kommunikasjon|Not found||
|Inger Hvarnes, Sandefjord|Plenum Sandefjord AS|Not found||
|Per Sørum, Trondheim|Maintech AS|Not found||
|Gunnar Laukholm, Mo i Rana|Ingeniørgruppen AS|Not found||

SØK på navn skal ikkje gi treff på kontaktpunkter med busmob3

!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values |contain number?| actual values? |
|Astrid Ullern, Atrå|98282848|Not found||
|Jarle Lilletvedt, Nesttun|92400003|Not found||
|Olav Dahl, Drammen|93487025|Not found||
|Diplom-Is AS rakkestad|69225047|Not found||
|Aktieselskapet Vinmonopolet, Drammen|32893502|Not found||
|Fagteam Gjøvik, Gjøvik|61109490|Not found||
|Hans Bjarne Holmen AS, Mosjøen|75174629|Not found||
|Avinor AS, Bodø|75520391|Not found||
|Tommy Skarvik, Rognan|97641534|Not found||
|Hvaler Sokn, Vesterøy|69376550|Not found||
|Nav Buskerud, Drammen|32828833|Not found||
|Martins agenturer AS, Ski|90042535|Not found||
|Nordlandssykehuset HF, Stokmarknes|76150072|Not found||
|Mann Oasen AS, Fyllingsdalen|55166964|Not found||
|Borg forsikring, Rolvsøy|69326940|Not found||
|Morris-Accent, Ålesund|70126439|Not found||
|Det Kongelige Hoff, OSLO|22048911|Not found||
|Spisestuen, Haugesund|99468551|Not found||
|Hegnar hotell as, nesbru|66772600|Not found||
|Skippertorget AS, Sandefjord|33473919|Not found||
|Alliance apotek Løven, Arendal|37025472|Not found||
|Einar Vårvik, Skudeneshavn|52829095|Not found||
|Bbe Mote AS, Karmsund|52855283|Not found||
|Det Gule Huset, Høvik|95476685|Not found||
|Kenneth Karlberg, Oslo|93438036|Not found||
|Thomas Hafsengen, Oslo|93438025|Not found||
|Kurt Jørgensen, Sandefjord|90672507|Not found||
|Astrid Merethe Svele, Oslo|48990145|Not found||
|Sydvaranger Gruve AS, Kirkenes|45257151|Not found||
|Elkem AS, Sandhornøy|75757406|Not found||
|Gisle Bellika, Lørenskog|97035870|Not found||
|Pål Porsmyr, Rolvsøy|90557146|Not found||
|T Angen Kåpesenteret AS, Jessheim|63973086|Not found||
|kirkegata optikk AS, Stavanger|51891901|Not found||
|Geir Kristian Mortensen, Tromsø|92031392|Not found||
|Kjell Hall AS, Rud|21393944|Not found||
|Ringerike kirkelige fellesråd, Hønefoss|90954306|Not found||
|Arnstein Aas, Holmestrand|92467980|Not found||
|Bård Y Kvamme, Haukeland|40604903|Not found||
|AS Reservedeler, hemnes|63850300|Not found||
|Fremover AS, Dronningensgate Narvik|91547083|Not found||
|Einar Hordnes, Fridtjof Nansens veg Gardermoen|41423006|Not found||
|Lege Angelika Kerane, Christianslund alle|69319839|Not found||
|Petter Nekså, Trondheim|92606519|Not found||
|Regus AS, Karenlyst alle Oslo|23120638|Not found||
|King food AS, Straume|56333697|Not found||
|Saltimport AS, Sandviksstranden Bergen|92260830|Not found||
|YX Grini, Eiksmarka|67144871|Not found||
|Hakim Panatar, Skedsmokorset|92815571|Not found||
|Kjell Lauritzen, Dronningens gate Oslo|97599360|Not found||
|Tom Erik Øvreby, Kløfta|95441277|Not found||
|Harald Gunnar Lima, Bryne|51483344|Not found||
|Ewie Samuelsen, Sarpsborg|91681718|Not found||
|Smykkebutikk AS, Stoltenbergs gate Tønsberg|33334443|Not found||
|Kristin Lilleskare, Kanalveien Bergen|98284503|Not found||
|Einar Jarle Våbenø Lade alle Trondheim|48400421|Not found||
|Øyvind Larsen Karihaugveien Oslo|92236412|Not found||
|Afrodite Stor Mote Ane Birte Lie, Mandal|38263818|Not found||
|Ingrid-Helene Steinsland, Sandnes|99297898|Not found||
|Top-Toy Norge AS, Haugesund|52731227|Not found||
|Anne Cecilie Rinde, Gjøvik|92202182|Not found||
|Tore Weberg, Fosser|41515500|Not found||
|Musikkservice, Egersund|47289453|Not found||
|JYSK AS, Brønnøysund|75023370|Not found||
|Nicholas Neill Staurland |95757736|Not found||
|Vakt service AS - Telemetri, Tønsberg|97787708|Not found||
|Satpos AS, Aukra|85424901|Not found||
|rælingen videregående skole|90505348|Not found||
|Porsanger kommune|78463599|Not found||
|brønnøy kommune|75012552|Not found||
