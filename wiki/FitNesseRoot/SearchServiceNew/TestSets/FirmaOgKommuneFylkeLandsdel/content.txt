!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values |contain top name and postal area?| actual values? |
|helfo, vestlandet|HELFO region vest Sola|2||
|Coop, bærum|COOP Prix Løkeberg Høvik ¤ COOP Prix Tanum Slependen ¤ COOP Mega Bekkestua Bekkestua ¤ COOP Extra Rykkinn Rykkinn|12||
|Ranheim legesenter, trondheim|Ranheim legesenter Ranheim ¤ Eugen Eide AS Ranheim|2||
|pers hotell, hallingdal|Pers Hotell AS Gol ¤ Oline Spa Pers Resort Gol ¤ Tropicana badeland Gol ¤ Narvesen Gol|4||Datagrunnlaget. |
|orion nordland|Lille Orion Bodø ¤ Orion pizza restaurant AS Bodø ¤ Lille Orion pizza og kebab Bar AS Fauske|3||
|orstad legesenter bryne|Orstad legesenter klepp stasjon|1||
|orstad legesenter jæren|Orstad legesenter klepp stasjon|1||
|sas ullensaker|SAS Norge Gardermoen ¤ SAS ground services Gardermoen ¤ SAS cargo norway as Gardermoen|3||
|Lillehammer bil oppland|lillehammer bil as Lillehammer ¤ møller bil lillehammer as Lillehammer|2||
|Mekonomen nordre land|Mekonomen Dokka bilverksted Dokka|1||
|bergfossenteret gjøvik|Bergfossenteret Dokka|1||
|elkjøp øvre romerike|Elkjøp Stormarked Jessheim Jessheim ¤ Elkjøp Sven Bråthen AS Årnes|2||
|Prøven bil midtnorge|Prøven bil AS Trondheim ¤ Prøven Bil AS Orkanger ¤ Prøven Bil Verdal AS Verdal ¤ Prøven bil Oppdal AS Oppdal|4||
|Rica grand Hotell Troms|Rica Grand Nordic Hotel Harstad ¤ Rica grand hotel Tromsø ¤ rica hotel svolvær Svolvær ¤ rica hotel narvik Narvik ¤ rica hotel bodø Bodø|5||
|Nav østensjø østlandet|NAV Østensjø Oslo ¤ NAV Østensjø sosialtjenesten Oslo ¤ Østensjø boligkontor Oslo|3||
|fannrem legesenter midt norge|Fannrem legesenter ANS Fannrem|1||
|hardanger bygg vestlandet|Hardanger Bygg AS Norheimsund ¤ Hardanger utvikling Norheimsund ¤ Hardanger Grunn & Naturstein AS Øystese ¤ Hardanger Laft AS Granvin|4||
|Go-Kart Utleige Vestlandet|Go-Kart Utleige Ålesund Eidsnes|1||
|Kokkens restaurant Østlandet|Kokkens restaurant AS Hønefoss ¤ Kokkens Hønefoss|2||
|Bærum kommune Østlandet|Bærum kommune Sandvika|1||
|Intersport Liertoppen Østlandet|Intersport Liertoppen AS Lierskogen|1||
|Partrederiet Ventura Vestlandet|Partrederiet Ventura DA Leinøy|1||
|Bilbroker AS Østlandet|Bilbroker AS Lysaker|1||
|Norges Varemesse Østlandet|Norges Varemesse Lillestrøm|1||
|Arve Kvilås Midt-Norge|Kvilås Arve Limingen|1||Gir 0 treff i prod. Finnes Arve Kvilås Limingen (Tlf 74335377) og Arve Kvilaas Limingen (Mob 92637081)|
|Lars Winum Hilding Midt-Norge|Winum Lars Hilding Hegra|1||
|Fenstad Spa AS Østlandet|Fenstad Spa AS Fenstad|1||
|Powerevent AS Østlandet|Powerevent AS Skedsmokorset|1||
|Eggedal samfunnshus Østlandet|Eggedal samfunnshus og svømmehall Eggedal|1||
|Boiesen Jan Østlandet|Boiesen Jan Lillestrøm|1||
|Interval Finland Østlandet|Interval International Finland Nesbru|1||
|Bjoneroa Kulturbygg Østlandet|Bjoneroa Kulturbygg AS Bjoneroa|1||
|Hurtigruteekspedisjonen Vestlandet|Hurtigruteekspedisjonen Kristiansund Kristiansund N|1||Gir 0-treff i prod|
|Transportsentralen Finnsnes AS Nord Norge|Transportsentralen Finnsnes AS Finnsnes|7||I prod komme denne på 8.plass, de 2 første treffene er Taxi Midt Norge AS|
|Skei Skule Vestlandet|Skei Skule Skei i Jølster|1||
|Kristoffer Tung Midt-Norge|Tung Kristoffer Stadsbygd|2||1 privat og 1 firma|
|Nordahl consult Østlandet|Nordahl consult Jessheim|1||
|Fredrik Alden Vestlandet|Alden Fredrik Værlandet|1||
|Opdahl AS Nord-Norge|Opdahl AS Varangerbotn|1||
|Hoseth service AS Vestlandet|Hoseth service AS Meisingset|1||
|Sverre Pettersen AS Midt-Norge|Sverre Pettersen AS Opphaug|1||
|Nordisk dekk import AS Vestlandet|Nordisk dekk import AS Avaldsnes ¤ Nordisk dekk import AS Nyborg|2||
|Klinikk for Deg Østlandet|Klinikk for Deg Dyrnes Inger-Johanne Rasta|1||Gir 0-treff i prod - fallback|
|Gulspurven barnehage Østlandet|Gulspurven barnehage Filtvet|1||
|Døli skole Østlandet|Døli skole Jessheim|1||
|Billigbil AS Vestlandet|Billigbil AS Knarrevik |1||
|Grand hotel AS Vestlandet|Grand hotell Hellesylt AS Ålesund|7||
|Solbu Utvikling Østlandet|Solbu Utvikling Haugan Geir Flatdal|1||
|EiendomsMegler 1 Årnes Østlandet|EiendomsMegler 1 Årnes Årnes|1||
|Golax Sveiseservice AS Østlandet|Golax Sveiseservice AS Gressvik|1||
|Damsgårds klinikk Vestlandet|Damsgårds klinikk Laksevåg|1||
|JGB Corporate AS Østlandet|JGB Corporate AS Snarøya|1||
|MS Fjell og Veisikring AS Vestlandet|MS Fjell og Veisikring AS Svortevik|1||
|Herøynytt AS Vestlandet|Herøynytt AS Fosnavåg|1||
|Klokkerhaugen camping Midt-Norge|Klokkerhaugen camping Singsås|1||
|DEFA AS Østlandet|DEFA AS Nesbyen|2||
|Eikeleina AS Østlandet|Eikeleina AS Slemmestad|1||
|Leos Elektronikk Østlandet|Leo`s Elektronikk Olsen Svein Arild Ørje|1||
|Røyken kraft Østlandet|Røyken Kraft AS Drammen|1||
|Tokke hjemmesykepleie Østlandet|Tokke hjemmesykepleie Dalen|1||
|Geilo legesenter Østlandet|Geilo legesenter Geilo|1||
|Moltu Skule Vestlandet|Moltu Skule Moltustranda|1||
|Jar skole Østlandet|Jar skole Jar|1||
|Stokkebø entreprenør Einar Vestlandet|Stokkebø entreprenør Einar Vangsnes|1||
|EXBO eiendom Lund Sørlandet|EXBO eiendom Lund Kristiansand S|1||
|Landstil AS Vestlandet|Landstil AS Moi|2||
|Roligheden barnehage Sørlandet|Roligheden barnehage Kristiansand S|1||
|Nordby ungdomsskole Østlandet|Nordby ungdomsskole Jessheim|1||
|Polaris World Norway AS Østlandet|Polaris World Norway AS Lysaker|1||
|Polaris barnehage AS Østlandet|Polaris barnehage AS Raufoss|1||
|Notabene Sannidal Østlandet|Notabene Sannidal Sannidal|1||
|Vigrestad dør & kjøkken AS Vestlandet|Vigrestad dør & kjøkken AS Vigrestad|1||
|Melhus kommune Midt-Norge|Melhus kommune Melhus|1||
|Bilpleieren Vestlandet|Bilpleieren Ålesund Ålesund|2||
|Malermester Espetvedt & Graawe AS Vestlandet|Malermester Espetvedt & Graawe AS Florvåg|2||
|Nav Sogn og Fjordane Vestlandet|Nav Sogn og Fjordane Leikanger|1||
|Skedsmokorset Tannhelsesenter Østlandet|Skedsmokorset Tannhelsesenter Skedsmokorset|1||
|T Sylte Bil AS Vestlandet|T Sylte Bil AS Surna|1||
|Folkets hus Moelv Østlandet|Folkets hus Moelv Moelv|1||
|Lia-tunet Furuli Østlandet|Lia-tunet Margrethe Bjørnstad Furuli Hernes|1||
|Gjensidige forsikring vestfold|Gjensidige forsikring Tønsberg ¤ Gjensidige forsikring Horten ¤ Gjensidige forsikring Larvik|3||
|Sykehusfrisøren agder|Sykehusfrisøren V/Mette Dragvik Sordal Kristiansand S|1||



!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values |contain ranked name?|
|eiker dyreklinikk nedre eiker|Eiker Dyreklinikk ¤ Krokstad senter|Found ranked| actual values? |
|curato bergen bergen|Curato Røntgen Bergen ¤ Bergen Storsenter|Found ranked|
|rema, kvam|Rema 1000 Norheimsund ¤ Rema 1000|Found ranked|
|elkjøp molde vestlandet|Elkjøp Stormarked molde ¤ Elkjøp Stormarked AS|Found ranked|
|Intersport Liertoppen Østlandet|Intersport Liertoppen AS ¤ Liertoppen kjøpesenter|Found ranked|
