|${SEARCH_API} check top x name|
|query |expected |checkTop |result? |kommentar|
|livilysen@hotmail.com|Liv Ingvild Lysen|1|||
|einangsa@hotmail.com|Anne Birgitte Einangshaug|1|||
|jangunnar.aasnes@gmail.no|Jan Gunnar Aasnes|1|||
|anerik@online.no|Andreas Eriksen|1||Skal komme på førsteplass|
|funkyt@bluezone.no|Tilleskjør Tomm|1|||
|trine@etageinterior.no|Etage interiør Trine Engh|1||Burde gitt denne først. OK på 1881.no, IOK i panorama|
|bolt@online.no|Wiggo Boltland|1||Skal komme på førsteplass|
|georg.in@online.no|Georg Wilhelm Tømrer Indreeide|1|||
|braaten@online.no|Tom Bråthen|2||Skal komme blant de 2 første|
|bjornsj_81@hotmail.com|Bjørn Erik Sem-Jacobsen|1|||
|post@sommerro.no|Sommerro tannklinikk Bettina Slyngstadli|1||Skal komme på førsteplass|
|stavanger@hydrotexaco.no|Helge Barka|1||Skal komme på førsteplass|
|nilsbj@nenett.no|Nils Bjørnflaten|1|||
|bo@lillealter.no|Bjørn Olav Lillealter|1||Skal komme på førsteplass|
|Terje.Skagseth@smn.no|Terje Skagseth|1|||
|nav.kragero@nav.no|NAV Kragerø|1|||
|florvag.barnehage@askoy.kommune.no|Florvåg barnehage|1|||
|kne-barn@online.no|Knerten barnehage|1|||
|siv@fanabarnehage.no|Fana barnehage AS|1||Skal komme på førsteplass|
|kvalbarn@frisurf.no|Kvalavåg Vel korttidsbarnehage|1|||
|post@kompetanse-regnskap.no|Kompetanse regnskap AS|1|||
|booking@casasheddy.com|Casas Heddy norsk AS|1||Skal komme på førsteplass|
|apollon@mestermur.as|T.A. Hemstad Murmester og Entreprenørforretning|1|||
|post@ns.no|Nordic solutions AS|2||Skal komme på førsteplass|
|post@agderport.no|Agder-Port AS|1||Skal komme på førsteplass|
|medskapende@iml.no|IML - Institutt for medskapende ledelse|1|||
|post@polartours.no|Polar Tours AS|1||Skal komme på førsteplass|
|sentrumblomster@gmail.com|Sentrum Blomster|1||Skal komme på førsteplass|
|post@fjermestad-as.no|Fjermestad AS|1|||
|morten.larsen@mrfylke.no|Fylkestannlegen i Møre og Romsdal|1|||
|trine.eikenes@em1mn.no|EiendomsMegler 1 avd St. Olavsplass|1||Treffer enheten. I enheten(manuelle gruppa) finnes kontaktpunktet Trine Paulsen Eikenes som er eiger av epost|
|kontoret@glemmen.no|Glemmen Menighet|1|||
|rita.granum@praksisregnskap.no|PraksisRegnskap|1|||
|post@swtrykk.no|Sæthren & Wraamann AS|1||Skal komme på førsteplass|
|nittedal@bakeriet.net|Bakeriet Cafedrift|1|||
|post@scenefinnmark.no|Scene Finnmark|1||Skal komme på førsteplass|
|buskerud@hoyre.no|Buskerud Høyre|1||Skal komme på førsteplass|
|firmapost@maxitaxitromso.no|Maxitaxi Tromsø|1|||
|mandal@norskflid.no|Vestagder Husflid AS|1||Skal komme på førsteplass|
|post@oslolappen.no|Oslolappen Autorisert trafikkskole|1||Skal komme på førsteplass|
|linne@thonhotels.no|Thon Hotel Linne|1||Skal komme på førsteplass|
|post@kanfrisor.no|Kan Frisør RingnesPark|3||Skal komme på førsteplass|
|post@glommapizzapub.no|Glomma Pizza og Pub|1||Skal komme på førsteplass|
|haj@weld.no|Jordalen Consulting AS|1|||
|95760482@online.no|Jordalen Consulting AS|1|||
|trondheim@megaflis.no|MegaFlis Trondheim|1|||
|gunnstor@online.no|Storli & Stornes AS|1||Skal komme på førsteplass|
|postlavendel@yahoo.no|Lavendel hud og velvære|1|||
|post@stadionparken.no|Stadionparken|1||Skal komme på førsteplass|
|amanda@steenstrom.com|Amanda senter|1|||
|pyramiden@amfi.no|AMFI pyramiden|1|||
|jekta@nord.coop.no|Jekta storsenter|1||Skal komme på førsteplass|
|http://www.ogge.no|Alfred Fjermeros|1||Skal komme på førsteplass|
|golli-service.no|Ismat Golli|1|||
|http://www.bjorkearkitektur.com|Steffen Wellinger|1|||
|http://www.facebook.com/supermajnn|Børge Storheil|1|||
|www.facebook.com/andreasskogen|Andreas Skogen|1|||
|www.fergebilder.net|Ronny H Kvande|1|||
|http://www.myridesign.com|Andre Myrlønn|1|||
|nidarosinkasso.no/|Monica Helberg Lastra|2||Skal komme på førsteplass|
|http://www.krageroresort.no|Kragerø Resort|1|||
|saekabel.no|Lars Steinar Sperling|1|||
|dinbemanning.no|Jonas Voldhagen Myrvold ¤ AS Din bemanning|2||din bemanning må på topp|
|http://www.vest.li/|Audun Daniel Lindbråten|1|||
|www.hms-mc.no/|Svein Erik Nilsen|2|||
|glennlyse.com/|Glenn Lyse|1|||
|http://www.facebook.com/profile.php?id=730596281|Helge Tverdal|1|||
|www.havern.no|Ingvill Warholm|1|||
|linkedin.com/in/bardhaugerud|Bård Haugerud|1|||
|http://www.johannesberggren.com/|Johannes Berggren|1|||
|http://www.dosvig.no/|Cecilie Galtung Døsvig|1|||
|varodd.no|Jonny Ånensen|2|||
|www.asbjornpettersen.com|Asbjørn Pettersen|1|||
|https://www.facebook.com/andre.korsnes|Andre Korsnes|1|||
|merefoto.no|Merethe Sundt|1|||
|http://www.fengselet.no|Fengselet Mat & Vinkjellar|1|||
|http://www.treningsverkstedet.no|Christine Thune|1|||
|http://www.nrp.as/|Nrp business Management AS|2|||
|http://www.lottasko.no/|Lotta sko AS|1|||
|http://www.hundesetra.com/|Gml Hundesætra AS|1|||
|http://www.123advokaten.no/|1-2-3 Advokaten AS|2|||
|http://www.barebilutleie.no/|Bare bilutleie AS|1|||
|http://www.parykksalongen.no/|Parykksalongen AS|1|||
|http://www.feiringkattepensjonat.com|Feiring Kattepensjonat|1|||
|http://www.achilles.no|Achilles Development services AS|4|||
|http://www.intersonic.no|Intersonic AS|1|||
|http://www.btmembran.no|Bærum Tak og Membran AS|1|||
|http://www.derdubor.no/toreholm/|Tore Holm & sønn AS|1|||
|http://www.pilconsulting.no/|Pil consulting AS|1|||
|http://www.skogplanter.no/|Skogplanter Østnorge AS|1|||
|http://www.witec.no/|Witec Hydraulics AS|1|||
|http://www.kolvereidfamiliecamping.no/|Kolvereid Familiecamping|1|||
|facebook.com/pages/KAN-art-fris%C3%B8r/173229210967|Kan Art Frisør|1|||
|http://www.facebook.com/KanFrisorVika|Kan Frisør Vika|1|||
|http://www.facebook.com/jushjelpa|Jushjelpa i Midt-Norge|1|||
|http://www.lanekassen.no/|Statens Lånekasse for utdanning|1|||
|http://www.jafshorten.no/|JaFs Horten|1|||
|http://www.geirjgulv.com|Geir Jonassen|1|||
|http://www.jenshoff.no/|Jens Hoff Garn & Idè - Heimdal|5|||
|http://www.divan.no/|Divan Pizzeria & Hamburgerbar|1|||
|http://www.jenshoff.no/|Jens Hoff Garn & Idè - City Syd|5|||
|http://www.sykehuset-innlandet.no/|Sykehuset Innlandet Hadeland|11|||
|http://franskenytelser.no/|Franske Nytelser Lade|1|||
|www.thermax.rl.no/|Thermax AS|1|||
|www.awilhelmsen.no|Awilhelmsen Management AS|1|||
|http://tonsbergtaxi.no|Viken Horten taxi|3||Finnes 3 stk. Kan være OK at den treffer som nr 4 også, siden RingTaxi 02393 alltd skal komme på 2 plass når ordet taxi blir søkt|
|greif.no|Greif Norway AS|1|||
|http://www.facebook.com/baklinikken/|BA Klinikken|1|||
|http://www.haugesundblikk.no/|Haugesund Blikk AS|1|||
|http://www.oceanteam.nl/|Oceanteam Shipping ASA|1|||
|http://www.trioregnskapas.no|Trio regnskap AS|1|||
|http://www.sawatdeethai.no/|Sawatdee Thai restaurant AS|1|||
|http://www.hackit.no/|Hackit Norway AS|1|||
|http://www.hinkenhopp.no/|HinkenHopp|1|||
|http://www.matbox.no/|Matbox|1|||
|http://www.huser.as/|Huser Entreprenør AS|1|||
|http://www.sweco.no/|Sweco Norge Avd Kjeller|17|||
|http://www.finnskogene.no/|Dæsbekken Villmarksenter|4|||
|http://www.bowling1.no/|Mix Os|8|||
|http://www.politi.no/|Sandnes politistasjon|1||Finnes så mangen, at denne kan komme etter treff 25 også. Vil da stå som 9999. Må da verifiseres på andre måter|
|nortura.no/|Nortura Rudshøgda|25||Verifisere at det gir treff på en avdeling (Finnes 25 stk)|
|http://www.nortura.no/|Nortura SA|1||Hovedkontoret skal komme først, for den er flagget. Alle flaggede enheter skal ha høgare boost uansett søk|
|http://www.vinmonopolet.no/|Vinmonopolet AS|1||Hovedkontoret skal komme først, for den er flagget. Alle flaggede enheter skal ha høgare boost uansett søk|
|www.kloverhuset.com|Kløverhuset|1|||
|www.oslocity.no|Oslo City|1|||
|www.storostorsenter.no|Storo Storsenter|1|||
|www.manglerudsenter.no|manglerud senter|2||OBOS Forretningsbygg AS har også denne url'n|
|www.sandvika-storsenter.no/|sandvika storsenter|1|||
|www.m44.no|M44|1||Skal komme på førsteplass|
|www.stadionparken.no|Stadionparken|1|||
|www.amfimadla.no|Amfi Madla|1|||
|http://www.amanda.no|Amanda senter|1||Skal komme på førsteplass|
|fotografaase.no|Aase AS|1|||
|inl@1881.no|Ingvild Leivestad|1|||
|www.1881.no|Ingvild Leivestad|20|||
