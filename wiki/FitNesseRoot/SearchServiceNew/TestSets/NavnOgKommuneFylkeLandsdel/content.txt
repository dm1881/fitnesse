!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values |contain top name and postal area?| actual values? |
|aud leivestad vestlandet|Aud Hillesvik Leivestad Norheimsund|2||
|Mary anne Solberg Vestlandet|Mary-Anne Solberg Strandebarm|1||
|aud leivestad hardangerfjordvegen 281 vestlandet|Aud Hillesvik Leivestad Norheimsund|1||
|Ranveig Lokøy Bergen|Ranveig Lokøy Fyllingsdalen|1||
|Stig Ersvær Bergen|Stig Ersvær Fyllingsdalen|1||
|Torunn Paulsen Brandt Bærum|Torunn Paulsen Brandt Jar|1||
|Linda Bolle Dyrøy|Linda Bolle Brøstadbotn|1||
|Bård Amundsen Vikna|Bård Amundsen Rørvik|1||
|Arnstein Oddgeir Saltnes Skaun|Arnstein Oddgeir Saltnes Buvika|1||
|Bjørg Randi Henriksen Tromsø|Bjørg Randi Henriksen Krokelvdalen|1||
|Georg Gloppen Bergen|Georg Gloppen Godvik|1||
|Karianne Engen Kjernsvik Bærum|Karianne Engen Kjernsvik Fornebu|1||
|Anders Speikland Smithsen Kristiansand|Anders Speikland Smithsen Kristiansand S|1||
|Tomm Alexander Skotner Asker|Tomm Alexander Skotner Billingstad|1||
|Jorunn Elise Stupforsmo Rana|Jorunn Elise Stupforsmo Storforshei|1||
|Anne Lisbeth Brekke Re|Anne Lisbeth Brekke Ramnes|1||
|Zenon Wachnik Frøya|Zenon Wachnik Swinoujscie|1||
|Hans Mikkelsen Ringsaker|Hans Mikkelsen Moelv|3||
|Johannes Melkevik Stord|Johannes Melkevik Sagvåg|1||
|Jakob Hessevik Vågsøy|Jakob Hessevik Bryggja|1||
|GEIR LEIVESTAD, kvam|Geir Leivestad Norheimsund |1||
|Magnus Oshaug Pedersen Trondheim|Magnus Oshaug Pedersen Tiller|1||
|Nina Stenhaug Stange|Nina Stenhaug Vallset|1||
|Rikke Kristine Eng Hamar|Rikke Kristine Eng Vang på Hedmarken|1||
|Elida Hegge Nord-Aurdal|Elida Hegge Leira i Valdres|1||
|Gunnar Morten Sandanger Sandnes|Gunnar Morten Sandanger Hommersåk|1||
|Arne Ingebrightsen Målselv|Arne I Ingebrigtsen Karlstad|1||
|Bjørn N Kollevåg Bergen|Bjørn N Kollevåg Bjørndalstræ|1||
|Turid Kristiansen Askøy|Turid Kristiansen Hauglandshella|1||
|Ngoc Thi Dieu Chan Skedsmo|Ngoc Thi Dieu Chan Skjetten|1||
|Ellinor D Bjørnstad Lørenskog|Ellinor D Bjørnstad Fjellhamar|1||
|Nina Weisser Pettersen Kristiansand|Nina Weisser Pettersen Kristiansand S|1||
|Oddbjørn Jenssen Storsteinnes Lyngen|Oddbjørn Jenssen Svensby|1||
|Toril Åsli Elden Vinnesgata Nedre Eiker|Toril Åsli Elden Solbergelva|1||
|Tore Bjune Heianveien Re|Tore Bjune Ramnes|1||
|Rafal Wisniewski Spelhaugen Bergen|Rafal Wisniewski Fyllingsdalen|1||
|Nina Kristin Grasmo Elvefaret Eidskog|Nina Kristin Grasmo Magnor|1||
|Hilde Pedersen Los Holthes G Hadsel|Hilde Pedersen Stokmarknes|1||
| Anna Barbara Pukala Flatevad Osterøy| Anna Barbara Pukala Lonevåg|1||
|Sabaheta Ramic Rundveien Sarpsborg|Sabaheta Ramic Hafslundsøy|1||
|Erik Forså Rabben 3B Bergen|Erik Forsaa Hordvik|1||
|Michael Levi Morgensolbakken 16 Nesodden|Michael Levi Fjellstrand|1||
|Hilde Steira Nordsetveien 2 Røyken|Hilde Steira Hyggen|1||
|Jorunn Ingrid Løtvedt Nordstrandveien 25C Hurum|Jorunn Ingrid Løtvedt Filtvet|1||
|Stian Yndestad Svendsen Titlestadvegen 11 Bergen|Stian Yndestad Svendsen Fana|1||
|Marius Mortensen Nordland|Marius Mortensen Alsvåg|3||
|Friid Nygard Egeli Buskerud|Friid Nygard Egeli Bødalen|1||
|Svenn Olav Storløs Nordland|Svenn Olav Storløs Mo i Rana|1||
|Einar Alfred Tverås Trøndelag|Einar Alfred Tverås Malm|1||
|Ida Fagerli Sør-Trøndelag|Ida Fagerli Svorkmo|1||
|Patricia Irene Branskome Rogaland|Patricia Irene Branscome Åkrehamn|1||fonetikk c-k|
|Mary Miranda Luther Buskerud|Mary Miranda Luther Tofte|1||
|Tom Talaksen Nordland|Tom Tallaksen Leknes|1||
|May Iren Korsnes Telemark|May Iren Korsnes Neslandsvatn|1||
|Przemyslaw Robert Piotrowski Buskerud|Przemyslaw Robert Piotrowski Åmot|1||
|Bjørnar Holmedal Østfold|Bjørnar Holmedal Rømskog ¤ Bjørnar Holmedahl Moss |2||
|Morten Lund, Sogn og Fjordane|Morten Lund Florø|1||Gir Morten Lund Tufte før Morten Lund i prod|
|Jan Reidar Rasmussen Sogn Fjordane|Jan Reidar Rasmussen Loen|1||
|Terje Wist Akershus|Terje Wist Finstadjordet|1||
|Magne Ivan Løvbrøtte Oppland|Magne Ivan Løvbrøtte Brandbu|1||
|Margith Fylling Hedmark|Margith Fylling Brumunddal|1||
|Elfrida Lauen Vest-Agder|Elfrida Lauen Tingvatn|1||
|Aud Marie Fiskvik Nord-Trøndelag|Aud Marie Fiskvik Skatval|1||
|Kjyrre Jørstad Rasmussen Buskerud|Kyrre Jørstad Rasmussen Krokkleiva|1||
|Erna Solfrid Bech Nordland|Erna Solfrid Bech Mo i Rana|1||
|Astrid Marie Egeland Rogaland|Astrid Marie Egeland Tananger|1||
|June Aslaug Eliassen Østfold|June Aslaug Eliassen Kråkerøy|1||
|Vegard Eivind Haukdal Sør Trøndelag|Vegard Eivind Haukdal Støren|1||
|Halvor Ognøy Rogaland|Halvor Ognøy Kopervik|1||
|Frid Inger Brekke Sør-Trøndelag|Frid Inger Brekke Leksa|1||
|Jens-Andreas Gude Akershus|Jens-Andreas Gude Heggedal|1||
|Marit Lund Larsen Østfold|Marit Lund Larsen Manstad|1||
|June Olsen Akershus|June Olsen Drøbak|1||
|Kåre Sund Buskerud|Kåre Johan Sund Kongsberg|1||
|Arne Hadland Orre Rogaland|Arne Hadland Orre Klepp stasjon|1||
|Bjørn Miljeteig Olssen Akershus|Bjørn Miljeteig-Olssen Lillestrøm|1||
|Erik Roen Akershus|Erik Roen Aurskog|1||
|Bjørn Eystein Krokdal Sør Trøndelag|Bjørn Eystein Krokdal Orkanger|1||
|Raymond Henry Birkedal Akershus|Raymond Henry Birkedal Rykkinn|1||
|Lise Nystad Rytter Akershus|Lise Nystad Rytter Kløfta|1||
|Karin Brit Pedersen Nord-Trøndelag|Karin Brit Pedersen Skogn|1||
|Cecilie Larsen Hordaland|Cecilie Larsen Hatlestrand|5||
|Unni Jenssen Sør-Trøndelag|Unni Jenssen Buvika|2||
|Dagny Marianne Borgen Troms|Dagny Marianne Borgen Sjøvegan|1||
|Jan Kåre Fiskerstrand Møre og Romsdal|Jan Kåre Fiskerstrand Fiskarstrand|1||
|Pinker Hordaland|Irene Pinker Olsvik|3||Irene pinker, irene landro pinker og remi landro pinker i hordaland|
|Jan-Gunnar Lie Hordaland|Jan-Gunnar Lie Godvik|1||
|Andreas Jenssen Hordaland|Andreas Jenssen Rådal|1||
|Steinar Sivertsen Nordland|Steinar Sivertsen Skonseng|2||
|Rolf Schou Agder|Rolf Schou Hægeland|2||To enheter på samme mann|
|Geir Vassbotn Akershus|Geir Vassbotn Drøbak|1||
|Anne Lise Linaker Akershus|Anne Lise Gusland Linaker Bekkestua|1||
|Asbjørn Kravik Buskerud|Asbjørn Kravik Krøderen|1||
|Helen Larsen Oppland|Helen Larsen Eina|1||
|Marianne Theres Ajer Østfold|Marianne Theres Ajer Sellebakk|1||
|Michael Pedersen Østfold|Michael Pedersen Moss|1||
|Edly, Akershus|Edly Balchen Drøbak|2||2 stk i Akershus med dette fornavnet|
|Odd Myrmæl Trøndelag|Odd Myrmæl Jerpstad|1||
|Ruben Pedersen Nordland|Ruben Pedersen Reine|2||
|Olar Mannik Rogaland|Olar Mannik Hafrsfjord|2||
|May Tangen Porsveien Sogn og Fjordane|May Tangen Sandane|1||
|Dag Bowitz Jøssangvegen Rogaland|Dag Bowitz Jørpeland|1||
|Elfrid Randi Rasmussen Dalheimslyngen Sør-Trøndelag|Elfrid Randi Rasmussen Jakobsli|1||
|Van Thi Ho Thu Hjalmar Brantings V Hordaland|Van Thi Ho Thu Fyllingsdalen|1||
|Johanne Abrahamsen Korsvikhaven Vest-Agder|Johanne Abrahamsen Kristiansand S|1||
|Ole Haga Jøssingv Akershus|Ole Haga Eiksmarka|1||
|Johannes Andreas Reksten Hordaland|Johannes Andreas Reksten Frekhaug|1||
|John Magne Dysjeland Uvikstrand Rogaland|John Magne Dysjeland Avaldsnes|1||
|Bjørg Dorthea Hjartnes Augestad Augastadvegen Hordaland|Bjørg Dorthea Hjartnes Augestad Tørvikbygd|1||
|Rita Emelie Burø Skjetnemarkv Sør-Trøndelag|Rita Emelie Burø Sjetnemarka|1||
|Kåre Andreas Meling Meling Hordaland|Kåre Andreas Meling Bremnes|1||
|Elina Maria Sundstrøm Laberg Troms|Elina Maria Sundstrøm Sjøvegan|1||
|Lill Harriet Bakke Rotsundv 896 Troms|Lill Harriet Bakke Rotsund|1||
|Ester Marie Bakka Ytre-Åsen 1 Rogaland|Ester Marie Bakka Suldalsosen|1||
|Alexander Tranberg Granliv 33 Telemark|Alexander Rørholt Tranberg Helle|1||
|Sigmund Johannes Solheim Smievegen 7 Sogn og Fjordane|Sigmund Johannes Solheim Florø|1||
|Einar Eilertsen Hegrevn 10 Nordland|Einar Eilertsen Svolvær|1||
|Tom Wollert-Nielsen Vidjeveien 81 Vestfold|Tom Wollert-Nielsen Tolvsrød|1||
|Maria Helena Norrie Ringveien 26 Troms|Maria Helena Norrie Nordkjosbotn|1||
|Frank-Robert Bæra Heggeliveien 3 A Østfold|Frank-Robert Bæra Mysen|1||
|Kari Therese Helverschou Hagen Tanumveien 59 C Akershus|Kari Therese Helverschou Hagen Slependen|1||
|Joakim Hagen Mårveg 20 Oppland|Joakim Hagen Hunndalen|1||
|Turid Kristin Bergman Kolstadtunet 6 B Sør Trøndelag|Turid Kristin Bergman Saupstad|1||
|Kjell Lode Kransberget 18 A Møre og Romsdal|Kjell Lode Elnesvågen|1||
|Anne Mette Østvold Hvalslia 10 Hedmark|Anne Mette Østvold Vallset|1||
|Arnt Erling Karlsen Helvik brygge 18 Vest-Agder|Arnt Erling Karlsen Vanse|1||
|Vera Bodil Nergård Berntsen NordNorge|Vera Bodil Nergård Berntsen Silsand|1||
|Thomas Iversen Stene Nord-Norge|Thomas Iversen Stene Brønnøysund|1||
|Øivind Røbech Vestlandet|Øivind Røbech Kristiansund N|1||
|Astrid Margrethe Lundø Skrunes Vestlandet|Astrid Margrethe Lundø Skrunes Mathopen|1||
|Arne Hans Steen Østlandet|Arne Hans Steen Helle|1||
|Ellen Støle Sjørbotten Østlandet|Ellen Støle Sjørbotten Tranby|1||
|Jorunn Langås Østlandet|Jorunn Langås Bærums Verk|1||
|Marit Ramstad Vestlandet|Marit Ramstad Hylkje|1||
|Alexander Martinsen Østlandet|Alexander Martinsen Gressvik|1||
|Solan Renolen, trøndelag|Solan Renolen Lundamo|1||
|Anneli Overholt NordNorge|Anneli Overholth Kabelvåg|1||
|Odd Agnar Hansen Nord-Norge|Odd Agnar Hansen Kvaløya|1||
|Solveig Liv Kristiansen MidtNorge|Solveig Liv Kristiansen Kattem|1||
|Målfrid Karin Tårneby Østlandet|Målfrid Karin Tårneby Vallset|1||
|Cato Altenborn Østlandet|Cato Altenborn Våler i Østfold|1||
|Charlotte Åkerøy Hundal Nord-Norge|Charlotte Åkerøy Hundal Herøy|1||
|Madeleine Gabriella Strøm-Blakstad Østlandet|Madeleine Gabriella Strøm-Blakstad Vestfossen|1||
|Joakim Mathisen Salomonsen Nord-Norge|Joakim Mathisen Salomonsen Leknes|1||
|Kine Kvam Østlandet|Kine Kvam Vang i Valdres|1||
|Anne Lise Berget Bjørnøy Vestlandet|Anne Lise Berget Bjørnøy Hafrsfjord|1||
|Joakim Bang Østlandet|Joakim Bang Flateby|1||
|Berit Kjelberget Østlandet|Berit Kjelberget Finstadjordet|1||
|Kjetil Luneng Vestlandet|Kjetil Luneng Arnatveit|1||
|Ellen Karin Røthin Nilsen Vestlandet|Ellen Karin Røthin Nilsen Skudeneshavn|1||
|Elvedin Vukotic Nord-Norge|Elvedin Vukotic Kirkenes|1||
|Rune Habbestad Vestlandet|Rune Habbestad Bremnes|1||
|Helga Haugen Østlandet|Helga Haugen Ryfoss ¤ Helga Haugen Kongsberg |6||
|Marte-Mari Hansen Østlandet|Marte-Mari Hansen Rolvsøy|1||
|Bjørn Enaasen Vestlandet|Bjørn Enaasen Kristiansund N|1||
|Per David Persson Vestlandet|Per David Persson Nesttun|1||
|Anne Skymoen Østlandet|Anne Skymoen Ilseng|1||
|Gerd Enger Østlandet|Gerd Enger Gaupen|4||
|Linn Therese Kollerød Østlandet|Linn Therese Kollerød Greåker|1||
|Ida Solheim Ringstad Midt Norge|Ida Solheim Ringstad Hegra|1||
|Anne Karin Isene Vestlandet|Anne Karin Isene Lote|1||
|Odd Ivar Mellum Østlandet|Odd Ivar Mellum Braskereidfoss|1||
|Bjørg Ellinor Dahl Vestlandet|Bjørg Ellinor Dahl Avaldsnes|1||
|Inger Anne Lyngroth Undheim Sørlandet|Inger Anne Lyngroth Undheim Kristiansand S|1||
|Brith Elin Henriksen Vestlandet|Brith Elin Henriksen Røyneberg|1||
|Håkon Espevik Vestlandet|Håkon Espevik Hagavik|1||
|Anita Huyen Ngo Østlandet|Anita Huyen Ngo Dilling|2||
|Laila Riska Vestlandet|Laila Riska Hommersåk|1||
|Roald Arne Larssen Nord-Norge|Roald Arne Larssen Andenes|1||
|Hege Johansen Frydenlund Østlandet|Hege Johansen Frydenlund Reinsvoll|1||
|Kjerstin Lindseth Vestlandet|Kjerstin Lindseth Os|1||
|Odd Ragnvald Tøften Østlandet|Odd Ragnvald Tøften Hærland|1||
|Terje Os Nesttun Østlandet|Terje Os Nesthun Furnes|1||
|Laila Sandvoll Østlandet|Laila Sandvold Stavern|1||
|Monica Helland Høvik Vestlandet|Monica Helland Høvik Uggdal|1||
|Ingar Oddleif Grøndal Lunavegen Østlandet|Ingar Oddleif Grøndahl Krokstadelva|1||
|Bjørg Solfred Woxen Fetveien Østlandet|Bjørg Solfred Woxen Fetsund|1||
|Janne Dagrun Teigen Braseth Bergvegen Midt-Norge|Janne Dagrun Teigen Braseth Hommelvik|1||
|Maren Anita Dyrvik Midt-Norge|Maren Anita Dyrvik Dyrvik|1||
|Martha Svendsen Østlandet|Martha Svendsen Magnor|1||
|Runar Sørbø Spove veien Vestlandet|Runar Sørbø Klepp stasjon|1||
|Torunn Lismoen Bleikenvegen Østlandet|Torunn Lismoen Brandbu|1||
|Evy Lindboe Fuglefjellvegen Østlandet|Evy Lindboe Årnes|1||
|Rune Henning Pettersen Hans Borgersens veg Østlandet|Rune Henning Pettersen Bøn|1||
|Inger Johanne Franson Homansvei Østlandet|Inger Johanne Fransson Blommenholm|1||
|Aslaug Amalia Pedersen Krokåsvegen Vestlandet|Aslaug Amalia Pedersen Kleppestø|1||
|Wojciech Przemyslaw Zawadzki Torpovegen Østlandet|Wojciech Przemyslaw Zawadzki Torpo|1||
|Jorn Jeppe Paulsen Belsetveien Østlandet|Jørn Jeppe Paulsen Rykkinn|1||
|Mads Gjesdal Kollbudalen 58 Vestlandet|Mads Gjesdal Morvik|1||
|Jan Erik Hagelund Kiseveg 480 Østlandet|Jan Erik Hagelund Nes på Hedmarken|1||
|Berit Johanne Fagerness Elveringen 7 Nord-Norge|Berit Johanne Fagerness Lakselv|1||
|Dawid Robert Dziuba Grosetveien 25 Østlandet|Dawid Robert Dziuba Slependen|1||
|Kamilla Annette Søttar Skjervengan 31 Nord-Norge|Camilla Anette Søttar Mosjøen|1||
|Zaher Hassani Daood Broveien 2 Sørlandet|Zaher Hassani Daood Kristiansand S|1||
|Marius Olsson Forsetveien 1 Østlandet|Marius Olsson Strømmen|1||
|Georgiana Dode Vardevn 109 Vestlandet|Georgiana Dode Fonnes|1||
|Bodil Marita Czybulla Egeland Mortevn 21 Vestlandet|Bodil Marita Czybulla Egeland Hundvåg|1||
|Tor Backe Halftan Jønssonsvei 2 D Østlandet|Tor Backe Bekkestua|1||
|Arne Otto Skutlaberg Hardangerfjord vegen 68 Vestlandet|Arne Otto Skutlaberg Norheimsund|1||
|Fredrik Gundersen Knudsmyr 17 Sørlandet|Fredrik Gundersen Flekkerøy|1||
|Lill Kristin Jenssen Frydenbergvei 4 A Østlandet|Lill Kristin Jenssen Strømmen|1||
|Tomas Hrdlicka Vestlandet|Tomas Hrdlicka Paradis|1||Gir 0 treff i prod - fallbacksøk|
|Øyvind Kammen Makrellsvingen Vestlandet|Øyvind Kammen Kristiansund N|1||Gir 0 treff i prod - fallbacksøk|
|Arild Bech Grunnavågen Vestlandet|Arild Bech Mosterhamn|1||Gir 0 treff i prod - fallbacksøk|
|Madeleine Ruud Mistelteinen Østlandet|Madeleine Ruud Tolvsrød|1||Gir 0 treff i prod - fallbacksøk|
|Karina Helen Torset Vollabakken Nord-Norge|Karina Helén Torset Halsa|1||Gir 0 treff i prod |
|Gintare Trapikaite Halsnøy Vestlandet|Gintare Trapikaite Sæbøvik|1||Gir 0 treff i prod - fallbacksøk|
|Tore Haugsdal Midtunbrekka Vestlandet|Tore Haugsdal Nesttun|1||Gir 0 treff i prod - fallbacksøk|
|mona elisabeth hansen, buskerud|Mona Elisabet Hansen Vestfossen |1||
|ingvild leivestad kvam kommune|Ingvild Leivestad|1||
|ane hennie lunner kommune|Ane Hennie|1||
