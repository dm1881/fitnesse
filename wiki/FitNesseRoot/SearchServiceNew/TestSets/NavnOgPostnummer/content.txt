!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values | contain top name and postal area? | actual values? |
| Bjørn Kåre Henriksen 5232 | Bjørn Kåre Henriksen Paradis |1| |
| Arvid Opsahl 2816 | Arvid Opsahl Gjøvik |1| |
| Kristian Sandtorv 5259 | Kristian Sandtorv Hjellestad |1| |
| Jakob Furuseth 4563 | Jakob Furuseth Borhaug |1| |
| Norunn Furuset 4563 | Norunn Furuseth Borhaug |1| |
| Anette Steinli 0488 | Anette Steinli Oslo |1| |
| Ronny Stadeløken 0488 | Ronny Stadeløkken Oslo |1| |
| Olav Ålberg 7042 | Olav Aalberg Trondheim |1| |
| Jan Andersen 3209 | Jan Eivind Andersen Sandefjord |1| |
| John Helge Møinichen 1368 | John Helge Møinichen Stabekk |1| |
| Britt Helene Neteland 1368 | Britt Helene Neteland Stabekk |1| |
| Gerd Anthonsen 4818 | Gerd Anthonsen Færvik |1| |
| Finn Erik Levin 3209 | Finn Erik Levin Sandefjord |1| |
| Ole Morten Løfsnæs 7049 | Ole Morten Løfsnæs Trondheim |1| |
| Knut Andersen 3209 | Knut H Andersen Sandefjord |1| |
| Frank Høybråten 0486 | Frank Høybråten Oslo |1| |
| Jon Sommervoll 0379 | Jon Sommervold Oslo |1| |
| Astrid Bergsholm 3160 | Astrid Bergsholm Stokke |1| |
| Hanne Kruse 4015 | Hanne Kruse Stavanger |1| |
| Marjatta Hanna Eliina Ala-Kahrakuusi 9770 | Marjatta Hanna Eliina Ala-Kahrakuusi Mehamn |1| |
| Harald Link 1618 | Harald Link Fredrikstad |1| |
| Annelin Engedal Berge 4056 | Annelin Engedal Berge Tananger |1| |
| Pål Doseth 2665 | Pål Doseth Lesja |1| |
| Hilde Dosett 2665 | Hilde Doseth Lesja |1| |
| Sigrun Doset 2665 | Sigrunn Doseth Lesja |1| |
| Synnøve Grimsby 1827 | Irene Synnøve Grimsby Hobøl |1| |
| Ingar Bjerkenås 7213 | Ingar Bjerkenås Gåsbakken |1| |
| Rolf kristian Hapling 0177 | Rolf  Christian Hapling Oslo |1| |
| Odd Bjarne Lønn 2013 | Odd Bjarne Lønn Skjetten |1| |
| Franz Pest 9996 | Franz Pest Utland |1| |
| Hans Asbjørn Brandsdal 2918 | Hans Asbjørn Brandsdal Fagernes |1| |
| Reidulf Lunga 1617 | Reidulf Lunga Fredrikstad |1| |
| Ole Gunnar Aase 3840 | Ole Gunnar Aase Seljord |1| |
| Angel Durlev 1812 | Angel Durlev Askim |1| |
| Nina Kobbeltvedt 5176 | Nina Kobbeltvedt Loddefjord |1| |
| Endre Olsen 4352 | Endre Olsen Kleppe |1| |
| May-Britt Grimås 1475 | May-Britt Grimås Finstadjordet |1| |
| Cato Egeland 4365 | Cato Egeland Nærbø |1| |
| Torleiv Ohma 4340 | Torleiv Ohma Bryne |1| |
| Trond Østlie 1415 | Trond Østlie Oppegård |1| |
| Mikal Sæteren 7340 | Mikal Sæteren Oppdal |1| |
| Solveig Huus 1472 | Solveig Huus Fjellhamar |1| |
| Kåre Skudal 3118 | Kåre Skudal Tønsberg |1| |
| Karoline Olsebakk Karlsen 2815 | Karoline Olsebakk Karlsen Gjøvik |1| |
| Håvard Thrones 1354 | Håvard Thrones Bærums verk |1| |
| Marianne Nielsen 0165 | Marianne Nielsen Oslo |1| |
| Unn Sætre Kalve 5385 | Unn Sætre Kalve Bakkasund |1| |
| Stian Meyer Hegerholm 1820 | Stian Meyer Hegerholm Spydeberg |1| |
| Heidi Aronsen 8622 | Heidi Aronsen Mo i Rana |1| |
| Hege Lunde 3142 | Hege Lunde Vestskogen |1| |
| Kai Erik Forsberg 2040 | Kai Erik Forsberg Kløfta |1| |
| Kjell Oddvar Vaeng 9300 | Kjell Oddvar Vaeng Finnsnes |1| |
| Mona Andersen 7040 | Mona Andersen Trondheim |1| |
| Ole Kjus 2014 | Ole Kjus Leirsund |1| |
| Turi Kjus 2015 | Turi Kjus Leirsund |1| |
| Margrete Aksnes 6894 | Margrete Aksnes Vangsnes |1| |
