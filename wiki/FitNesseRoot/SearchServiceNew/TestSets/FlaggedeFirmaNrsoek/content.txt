Flaggede firma skal også fungere ved nummersøk. Skal velge den flaggede enheten før de andre. Så sant ingen andre regler er satt.  OBS! En del feiler i prod pga Ved nummersøk så gir den manuelle grupper først, da nummeret gir treff 2 ganger i denne enheten pga dette (er ikkje tenkt at det skal fungere slikt)

!|${SEARCH_API} ${TABLE_NAME}|
|query |expected |checkTop |result? |
| 32266200 | A/S Elektrisk Produktion |1| |
| 23290000 | Adecco Norge AS |1| |
| 55349600 | Baker Brun AS |1| |
| 04045 | Bring |1| |
| 23010101 | Bull & Co Advokatfirma AS |1| | skal komme på første. ok på 1881.no |
| 92406060 | Chess mobil |1| |
| 24001000 | Dagbladet AS |1| |
| 81548260 | Euroflorist Norge AS |1| |
| 51837000 | Halliburton AS |1| |
| 05253 | Helse Fonna HF |1| |
| 48219000 | Kjedekontor - Bademiljø |1| |
| 05566 | Mekonomen Direkt (Kundeservice) |1| |
| 09919 | Nikita gruppen AS |1| |
| 91502121 | Relacom AS |1| | Kommer langt ned pga nummersøk som treff manuelle grupper = OK |
| 23155050 | Scandic Hotels AS |1| |
| 05400 | SAS Scandinavian Airlines System |3| | Her er det 3 enheter på dette nummer, som har flagg 1 satt |
| 80080000 | Skatt øst Oslo |1| |
| 80080000 | Skatteetaten |1| |
| 57638000 | Sogn og Fjordane Fylkeskommune |1| |
| 21025050 | SpareBank 1 Gruppen AS |1| |
| 72573000 | St.Olavs Hospital HF |1| |
| 55208000 | StS Gruppen |1| |
| 02000 | Tele2 Norge AS |1| |
| 81077000 | Telenor business solutions AS |4| | 4 kontaktpunkter har flagg satt. Denne |
| 92405050 | Telia Norge AS |1| | |
| 03080 | TINE Øst |1| | Kommer langt ned pga nummersøk som treff manuelle grupper = OK |
| 55598700 | Toma gruppen |1| |
| 72540000 | Trondheim kommune |1| |
| 05555 | Sparebanken Vest |1| |
| 22860300 | Toll- og avgiftsdirektoratet |1| |
| 22663030 | Tiger AS |2| | mobil oil kommer først pga manuell gruppe. IKKJE pga flagging. Både tiger og esso er flagga |
| 02422 | ICA Norge AS |1| |
| 71111000 | Molde kommune |1| |
| 06262 | Eidsiva Energi |1| |
| 81522040 | Cresco Kredittkort |1| |
| 33171000 | Larvik kommune |1| |

!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values | contain top name and postal area? | actual values? |
| 33420200 | Vestfold Taxisentral AS Sandefjord |1| | sandefjord på topp |
| 57757000 | Fjord1 AS Florø |1| | avd flor ø skal komme før molde |
| 04560 | Vinmonopolet AS Oslo |1| |
| 05505 | Tide Buss AS Bergen |1| |
| 05200 | Mesta AS Oslo |1| |
| 81544000 | IKEA AS Billingstad |1| |
| 02622 | COCA-Cola Norge AS Lørenskog |1| |
| 08855 | Norgesmøllene AS Nesttun |1| |
| 81500888 | NSB AS Oslo |1| | Flagg 1 |
| 81500888 | NSB AS Oslo S Oslo |2| | Flagg 2 |
