
!|${SEARCH_API} ${TABLE_NAME}|
| query | expected values |contain top name and postal area?| actual values? |
|Alta taxi 9510|Alta taxi AS Alta|1||
|Bodø energi 8006|Bodø energi AS bodø|1||
|brønnøysundregisteret 8900|Brønnøysundregistrene Brønnøysund |1||
|Dalen parkering 2060|Dalen parkering ANS Gardermoen|1||
|Danske bank 3019|Danske bank avd Drammen Drammen|1||
|Fokus bank 7800|Danske bank avd Namsos Namsos|1||
|fortum 1712|fortum Grålum|1||
|Fredrikstad sykehus 1606|sykehuset østfold Fredrikstad|1||
|Gjøvik taxi 2817|Gjøvik taxi Gjøvik|1||
|taxi 1767|Halden taxi AS Halden |2||RingTaxi på 1 plass, Halden Taxi AS på 2 plass i prod|
|Hammerfest sykehus 9600|Hammerfest sykehus Hammerfest|1||
|Haugesund sykehus 5528|Helse Fonna HF Haugesund|5||Kommer på 7 plass i prod. Finnes ein i porsgrunn som er innblanda i resultatet|
|Helgeland Sparebank 8980|Helgeland Sparebank Vega|1||
|ICE 0600|ICE Norge AS Oslo|1||
|kanal digital 1360|Canal digital norge as fornebu|1||
|Klinikk for alle 1400|Klinikk for alle Ski Ski|1||
|klp 5014|Kommunal Landspensjonskasse KLP Bergen|1||
|kongsvinger sykehus 2212|Sykehuset Innlandet Kongsvinger Kongsvinger|1||Kommer på 2 plass i prod|
|Lefdal 5239|Lefdal ElektroMarked Lagunen Rådal|1||
|legevakt Hamar 2318|Hedmarken Legevakt Hamar|1||
|Moss taxi 1531|Moss taxi as Moss|1||
|Møller bil 7044|Møller bil leangen Trondheim|1||
|NAV 5058|NAV Årstad Bergen|1||
|NAV 5004|NAV Hordaland Bergen|1||
|nav 3256|NAV Larvik Larvik|1||
|Nav 1723|NAV Forvaltning Østfold Sarpsborg|1||
|nav 3717|NAV Skien Skien|2||Nav Telemark og Nav skien har samme postnummer|
|Nav 9008|NAV Eures Troms Tromsø|7||7 stk med NAV i navn og postnr 9008|
|Network 4610|Network Norway AS Kristiansand S|1||
|Next Gentel 1366|NextGenTel AS Lysaker|1||
|nordea bank 2317|Nordea Hamar Hamar|1||
|Norges taxi 3511|Norgestaxi Hønefoss|1||
|obos 0180|OBOS Oslo|1||Riktig postnr er 0179 Oslo|
|oslo kommune 0160|Oslo kommune Oslo|1||
|sportslager 0183|Oslo Sportslager AS Oslo |1||
|Phonero 4016|Phonero Stavanger Stavanger|1||
|ringsaker kommune 2381|Ringsaker kommune Brumunddal|1||
|romerike taxi 2069|Øvre Romerike Taxi Jessheim|1||
|Sandefjord kommune 3202|Sandefjord kommune Sandefjord|1||
|Santander Bank 9405|Santander Consumer Bank AS Harstad|1||
|sektor alarm 2000|Sector Alarm AS Lillestrøm|1||
|Securitas 6018|Securitas AS Ålesund|1||
|sparebanken 6294|sparebanken møre fjørtoft|1||
|Star Tour 1368|Star Tour Stabekk|3||3 stk med samme navn og poststed. Men har ulike telefonnummre|
|sykehuset innlandet 2312|Sykehuset innlandet sanderud Ottestad|1||
|sykehuset østfold 1535|Sykehuset Østfold Moss Moss|1||
|taxi 8008|Nordland Taxi AS Bodø|4||Ringtaxi på topp, så 3 andre enheter med postnr 8008|
|Taxi Hamar 2330|Taxi Hedmark - Stange/Vallset Taxi Vallset|2||Ring taxi på topp|
|Telio 0275|Telio Oslo|1||
|vinmonopolet 5610|Vinmonopolet Norheimsund Norheimsund|1||
|Volvat 1608|Volvat medisinske senter Østfold AS Fredrikstad|1||
|Volvat oslo 0370|VitusApotek Volvat - Oslo Oslo ¤ Volvat medisinske senter AS Oslo|2||
|Rekneskapskontor 5640|Fjord'n Rekneskapskontor Eikelandsosen|1||
|Norsk Designråd 0182|Norsk Designråd Oslo|1||
|Flyttekonsulenten 2613|Flyttekonsulenten Lillehammer|1||
|Normisjon 2317|Normisjon Hamar|1||
|Fjordsylv 6893|Fjordsylv AS Vik i Sogn|1||
|SM Håndverkservice AS 1346|SM Håndverkservice AS Gjettum|1||
|Tollnes Murmester 1472|Thorbjørn Tollnes Murmester Fjellhamar|1||
|Abrahamsen 0354|Petter Abrahamsen Oslo|1||
|Fana Maskin 5228|Fana Maskin Nesttun|1||
|Lillevik Renseanlegg 3267|Lillevik Renseanlegg Larvik|1||
|Regnskap på Data 3679|Regnskap på Data Notodden|1||
