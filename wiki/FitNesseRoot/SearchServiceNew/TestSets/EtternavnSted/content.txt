|${SEARCH_API} check top x name|
|query |expected |checkTop |result? |kommentar|
|Berland, sandnes|mette berland¤ Egil Sigmund Berland¤ mariann berland|15|||
|Vetaas, alversund|tony vetaas¤  sølvi vetås¤ sidsel vetaas|15|||
|Skintveit, mathopen|signe skintveit¤ olav skintveit ¤ einar skintveit¤ johannes skintveit|7|||
|Skintveit, mathopen|Grethe skintveit ¤ margun beate skintveit|7|||
|Hungnes, vikebukt|Adolf Hungnes|2|||
|Sandvik, farsund|gunn sissel sandvik grimsby¤ arvid sandvik|6|||
|Germundson, kana|anders germundson|3|||
|Zibell, nesøya|sissel zibell¤ annicken zibell|3|||
|Arild, kongshus|geir anton arild ¤ stian arild|4|||
|Mandt, skien|fredrik andreas mandt¤ Tage-Mandt Utbøen¤ ingunn mandt|10|||
|Berget, åndalsnes|ragna lovise berget¤ kjell arne berget|6|||
|Tvedt, fana|christer tvedt¤ frode tvedt¤ kjersti sævold tvedt|8|||
|Bjørnflaten, notodden|Harald Bjørnflaten ¤ Tora Strand Bjørnflaten ¤ Sidsel Irene Bjørnflaten|6|||
|Huus-Hansen, sandefjord|erling huus-hansen|6|||
|Frøyseth, tynseth|siri grete frøyseth|1|||
|Kaldheim, oslo|aslaug alise kaldheim¤ maria kaldheim|2|||
|Vestrum, oslo|elin vestrum¤ jan tore vestrum¤ jie chen vestrum¤ ketil vestrum einarsen ¤ alexander vestrum|16|||
|Vestrum, oslo|Morten Vestrom ¤ Roar Westrum|15||Fonetikken tar Vestrom og Westrum, kommer fra 15 plass som er etter alle vestrum|
|Stabæk, skotselv|Bjørn Even Stabæk|1|||
|Stabæk, skotselv|Monica Stabæk ¤ harald stabæk¤ bjørn even stabæk ¤ Sissil Stabæk|5|||
|Viko, gol|hilde ingeborg viko¤helge viko ¤ Hans Kåre Viko ¤ Eva Sofie Viko|24|||
|Roka, oslo|caroline jeanette roka¤ tulendra bahadur roka|3|||
|Sæterdal, narvik|steinar sæterdal¤ ane solbakken sæterdal|3|||
|Gjellesvik, bergen|nina gjellesvik¤ Janicke Gjellesvik¤ eva margrethe gjellesvik|18|||
|Torp, miland|Kjetil Torp Songe¤ halvor torp songe|5|||
|Torp, miland|Espen Torp¤ Bjørg torp ¤ marte torp ¤ kjetil torp songe ¤ Bjørg Sigrid Johanne Bjerke Torp|5|||
|Almlid, oslo|espen almlid¤ elin almlid¤ angelica boholm almlid|4|||
|Foyn-Bruun, paradis|svend foyn-bruun|1|||
|rauvs, enskede|Olaf Rauws|1|||
|Børthus, reinsvoll|tom børthus¤  ronny børthus¤ lars neråsen børthus|16|||
|Aarvold, morvik|kjersti aarvold¤ anne karin årvoll|2|||
|Birkelund, tromsø|eirik birkelund¤ yngve birkelund¤ walter birkelund¤ knut einar birkelund|15|||
|Aarlie, kvål|oddny aarlie|1|||
|Søderholm, alta|arne søderholm¤ rikke søderholm¤ gerd aina søderholm|18|||
|Svendsgaard, trondheim|karen svendsgaard sæther¤ line helen svendsgaard|3|||
|Berntsen, nærbø|steinar berntsen¤ ingve benedikt berntsen|3|||
|Paulsberg, oslo|mats ulvund paulsberg¤ camilla grønsberg paulsberg¤ audun paulsberg|24|||
|Myhre, herøysundet|bente myhre|1|||
|Anderssen, bekkestua|eva anderssen¤ valborg lidemark anderssen|3|||
|Tollan, nøtterøy|arve tollan|1|||
|Wilhelmsen, hasvik|torbjørn wilhelmsen¤ anita soløy wilhelmsen|2|||
|Kolemeto, lysaker|wawina kolemeto|1|||
|Harr, alta|lise harr meedby¤ anne kari harr¤ mette harr meedby|4|||
|Martinussen, kvinesdal|tore martinussen¤ nancy ingrid strautins martinussen¤|2|||
|Birkeland, bokn|robert birkeland¤ inge birkeland¤ camilla holm birkeland ¤ venevil birkelund|4|||
|Vaccaro, oslo|frank alfred vaccaro¤ camilla linn vaccaro¤ britt hallem vaccaro|3|||
|Fjeldstad, lonevåg|aslaug fjeldstad|1|||
|Nørstebøen, etnedal|atle nørstebøen¤ Sigrun Oddny Nørstebøen Aune|3|||
|Hulaas, oslo|ingleif johan hulaas¤ trond ivar hulaas ¤ Reidun Hulaas¤Henrik Terje Hulaas|8|||
|Birkeland, trengereid|ida birkeland|1|||
|Lunder, lunner|Ingrid Lunder ¤  Per Lunder ¤ Halvard Lunder ¤ Ruth Synnøve Lunder|14|||
|Ebbestad, bødalen|ulrik ebbestad¤ geir qvam ebbestad|2|||
|Helskog, mosjøen|trine helskog¤ bjørn magne helskog¤ bente kristin helskog eriksen|6|||
|Bye, auli|rune bye¤ marte jensberg bye ulla|2|||
|Hansen, sjursnes|ole hansen¤ gustav hansen¤ anne marie elea hansen|7|||
|Huseby, hof|laila næs huseby¤ helge roar huseby|2|||
|Forseth, saupstad|ingrid forseth|1|||
|Simonsen, skulsfjord|elisabeth simonsen |2|||
|Leesland, oslo|roy egil leesland¤ imelda leesland¤ vivian leesland|10|||
|Knoll, Narvik|Siegfried knoll|1|||
|Jobran, bergen|elias a m jobran|1||161012 Han kommer på 13 plass, han har poststed Fyllingsdalen.|
|Westerberg, sømna|snorre westerberg¤ ellen westerberg¤ halvor olav westerberg|12|||
|Varvin, oslo|sverre varvin¤ stine varvin¤ kristoffer roti varvin ¤ scott varvin ravn|6|||
|Vedvik, drøbak|øyvind vedvik|1|||
|Hollund, stord|gunnar hollund¤ marie johanne hollund¤ alf egil hollund|6|||
|Gisholt, fornebu|jesper gisholt|1|||
|Haugen, rishøyhamn|henrik haugen¤ hermod jonny haugen¤ Camilla Haugen ¤Gerd Oddbjørg Haugen¤ingvill haugen vollan |8|||
|Hushovd, skien|mona hushovd¤ ronny hushovd|2|||
|Skogen, haugesund|Arnold Oddmar Skogen¤ geir skogen¤ borgny alise hauge skogen|17|||
|Haagensen, noresund|per haagensen|1|||
|Wabakken, fredrikstad|alexander wabakken¤ solveig ellionor wabakken|2|||
|Vaeng, finnsnes|kjell oddvar vaeng|1|||
|Tobiassen, tydal|solfrid tobiassen|1|||
|Lenngren, snarøya|tom lenngren¤ per ove lenngren|3|||
|Bostad, hadeland|thorstein bostad¤ sondre bostad|2|||
|jorung, hadeland|line jorung¤ glen jorung¤ sandra jorung|6|||
|jorung, harestua|line jorung¤ glen jorung¤ sandra jorung|5|||
|hennie, grua|tom hennie|2|||
|hennie, harestua|ane hennie|1|||
|harring, harestua|raymond harring¤ karianne harring¤ sissel harring|3|||
|Knoll, kolbotn|Heike knoll ¤ Dominika magdalena knol ¤ tore knoll ¤ anders sekkelsten knoll|5|||
|Vaeng, finnsnes|Simen Vaeng|3|||
|ganser, elverum|Klaus Friedrich Ganser ¤ Thorun Edel Ganser|3|||
|ganser elverum|Klaus Friedrich Ganser ¤ Thorun Edel Ganser|3|||
|haukås øystese norheimsund|Stig Arild Haukås Øystese|2|||
|øystese Norheimshagen|Lisbeth Øystese|3|||
|Leivestad gartveitvegen|Ingvild Levestad|1|||
|Leivestad øystese|Ingvild Leivestad|1|||