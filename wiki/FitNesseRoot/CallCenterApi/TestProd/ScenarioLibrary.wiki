---
Help: Scenario-bibliotek med variabler for prod-miljøet
---
!* Variabler for Prod-miljøet
!define SEARCH_API {callcenter}
!define SEARCH_TYPE {unit}
!define SEARCH_URL { https://callcenterapi.azure-api.net/search/${SEARCH_TYPE} }
#!define NUMBER_OF_RESULTS {10}
*!
!include <CallCenterApi.CallCenterScenariosBase