---
Help: Scenario-bibliotek for Callcenter-API. Rediger her!
---
!define SUBSCRIPTION_KEY {bc1497910af645388646254f2f718f56}

|Library | 
|string fixture |
|json http test |
|list fixture   |
|echo script    |

!3 Søk og sjekk enkeltfelter
Krever kolonner med overskrift: query, expected, checkTop og result?.
En siste kolonne med overskrift kommentar er valgfri.

!4 Telefonnummre
Leter etter gitt tlfnr i x antall øverste treff. Hvor x skal skrives i kolonnen med navn "checkTop".

|table template |callcenter check top x phone number |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$phoneNrs=           |list json path matches |$.contacts[:@{checkTop}].contactPoints[?(@.type=='Phone')].value |
|$mobilePhoneNrs=     |list json path matches |$.contacts[:@{checkTop}].contactPoints[?(@.type=='MobilePhone')].value |
|show |echo |$phoneNrs |
|show |echo |$mobilePhoneNrs |
|copy values from |$mobilePhoneNrs | to |$phoneNrs |
|show |echo |$phoneNrs |
|ensure | element | @{expected}  | is present in | $phoneNrs |
|$result=         |echo |$phoneNrs |


Sjekker at tlfnr IKKE er med i x øverste treff.

|table template |callcenter check top x phone number not |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$phoneNrs=           |list json path matches |$.contacts[:@{checkTop}].contactPoints[?(@.type=='Phone')].value |
|$mobilePhoneNrs=     |list json path matches |$.contacts[:@{checkTop}].contactPoints[?(@.type=='MobilePhone')].value |
|show |echo |$phoneNrs |
|show |echo |$mobilePhoneNrs |
|copy values from |$mobilePhoneNrs | to |$phoneNrs |
|show |echo |$phoneNrs |
|reject | element | @{expected}  | is present in | $phoneNrs |
|$result=         |echo |$phoneNrs |


!4 Navn, på personer og selskaper
Leter etter gitt navn i x antall øverste treff. Hvor x skal skrives i kolonnen med navn "checkTop".

|table template |callcenter check top x name |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$names=        |all json path matches |$.contacts[:@{checkTop}].name |
|$namesLow=	|convert to lower case | $names | |
|$expectedLow=	|convert to lower case | @{expected} | |
|ensure         |text |$namesLow |contains |$expectedLow |
|$result=       |all json path matches |$.contacts[:@{checkTop}].name |

Sjekker at navnet IKKE er med i x øverste treff.

|table template |callcenter check top x name not |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$names=        |all json path matches |$.contacts[:@{checkTop}].name |
|reject         |text |$names |contains |@{expected} |
|$result=       |all json path matches |$.contacts[:@{checkTop}].name |

!4 Hele adressen
Leter etter gitt adresse (med husNr, postNr, poststed) i x antall øverste treff. Hvor x skal skrives i kolonnen med navn "checkTop".

|table template |callcenter check top x street name |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$address=      |all json path matches |$.contacts[:@{checkTop}].geography.address.street |
|ensure         |text |$address |contains |@{expected} |
|$result=       |all json path matches |$.contacts[:@{checkTop}].geography.address.street |

!4 Adressetekst
Leter etter gitt adressetekst i x antall øverste treff. Hvor x skal skrives i kolonnen med navn "checkTop".

|table template |callcenter check top x address text |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$address=      |all json path matches |$.contacts[:@{checkTop}].geography.address.addressText |
|ensure         |text |$address |contains |@{expected} |
|$result=       |all json path matches |$.contacts[:@{checkTop}].geography.address.addressText |

!4 Streetname
Leter etter gitt gatenavn i x antall øverste treff. Hvor x skal skrives i kolonnen med navn "checkTop".

|table template |callcenter check top x full address |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$address=      |all json path matches |$.contacts[:@{checkTop}].geography.address.addressString |
|ensure         |text |$address |contains |@{expected} |
|$result=       |all json path matches |$.contacts[:@{checkTop}].geography.address.addressString |


!4 !4 Dokument-ID
Leter etter gitt dokID i x antall øverste treff. Hvor x skal skrives i kolonnen med navn "checkTop".

|table template |callcenter check top x id |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$id=           |all json path matches |$.contacts[:@{checkTop}].id |
|ensure         |text |$id |contains |@{expected} |
|$result=       |all json path matches |$.contacts[:@{checkTop}].id |

Sjekker at gitt ID IKKE er med i x øverste treff.

|table template |callcenter check top x id not |
|set value      |@{query}             |for |query      |
|set value      |${SUBSCRIPTION_KEY}  |for |subscription-key |
|get from       |${SEARCH_URL}        |
|show           |request |
|note           |@{kommentar} |
|$id=           |all json path matches |$.contacts[:@{checkTop}].id |
|reject         |text |$id |contains |@{expected} |
|$result=       |all json path matches |$.contacts[:@{checkTop}].id |